<?php


/**
 * Base class that represents a row from the 'album' table.
 *
 *
 *
 * @package    propel.generator.ORM.om
 */
abstract class BaseAlbum extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'AlbumPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        AlbumPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the title field.
     * @var        string
     */
    protected $title;

    /**
     * The value for the title_slug field.
     * @var        string
     */
    protected $title_slug;

    /**
     * The value for the description field.
     * @var        string
     */
    protected $description;

    /**
     * The value for the gallery_id field.
     * @var        int
     */
    protected $gallery_id;

    /**
     * The value for the cover_photo_id field.
     * @var        int
     */
    protected $cover_photo_id;

    /**
     * The value for the rank field.
     * Note: this column has a database default value of: 1
     * @var        int
     */
    protected $rank;

    /**
     * The value for the is_public field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $is_public;

    /**
     * The value for the max_width field.
     * @var        int
     */
    protected $max_width;

    /**
     * The value for the max_height field.
     * @var        int
     */
    protected $max_height;

    /**
     * @var        Gallery
     */
    protected $aGallery;

    /**
     * @var        PropelObjectCollection|Photo[] Collection to store aggregation of Photo objects.
     */
    protected $collPhotos;
    protected $collPhotosPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $photosScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->rank = 1;
        $this->is_public = 0;
    }

    /**
     * Initializes internal state of BaseAlbum object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [title] column value.
     *
     * @return string
     */
    public function getTitle()
    {

        return $this->title;
    }

    /**
     * Get the [title_slug] column value.
     *
     * @return string
     */
    public function getTitleSlug()
    {

        return $this->title_slug;
    }

    /**
     * Get the [description] column value.
     *
     * @return string
     */
    public function getDescription()
    {

        return $this->description;
    }

    /**
     * Get the [gallery_id] column value.
     *
     * @return int
     */
    public function getGalleryId()
    {

        return $this->gallery_id;
    }

    /**
     * Get the [cover_photo_id] column value.
     *
     * @return int
     */
    public function getCoverPhotoId()
    {

        return $this->cover_photo_id;
    }

    /**
     * Get the [rank] column value.
     *
     * @return int
     */
    public function getRank()
    {

        return $this->rank;
    }

    /**
     * Get the [is_public] column value.
     *
     * @return int
     */
    public function getIsPublic()
    {

        return $this->is_public;
    }

    /**
     * Get the [max_width] column value.
     *
     * @return int
     */
    public function getMaxWidth()
    {

        return $this->max_width;
    }

    /**
     * Get the [max_height] column value.
     *
     * @return int
     */
    public function getMaxHeight()
    {

        return $this->max_height;
    }

    /**
     * Set the value of [id] column.
     *
     * @param  int $v new value
     * @return Album The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = AlbumPeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [title] column.
     *
     * @param  string $v new value
     * @return Album The current object (for fluent API support)
     */
    public function setTitle($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->title !== $v) {
            $this->title = $v;
            $this->modifiedColumns[] = AlbumPeer::TITLE;
        }


        return $this;
    } // setTitle()

    /**
     * Set the value of [title_slug] column.
     *
     * @param  string $v new value
     * @return Album The current object (for fluent API support)
     */
    public function setTitleSlug($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->title_slug !== $v) {
            $this->title_slug = $v;
            $this->modifiedColumns[] = AlbumPeer::TITLE_SLUG;
        }


        return $this;
    } // setTitleSlug()

    /**
     * Set the value of [description] column.
     *
     * @param  string $v new value
     * @return Album The current object (for fluent API support)
     */
    public function setDescription($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->description !== $v) {
            $this->description = $v;
            $this->modifiedColumns[] = AlbumPeer::DESCRIPTION;
        }


        return $this;
    } // setDescription()

    /**
     * Set the value of [gallery_id] column.
     *
     * @param  int $v new value
     * @return Album The current object (for fluent API support)
     */
    public function setGalleryId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->gallery_id !== $v) {
            $this->gallery_id = $v;
            $this->modifiedColumns[] = AlbumPeer::GALLERY_ID;
        }

        if ($this->aGallery !== null && $this->aGallery->getId() !== $v) {
            $this->aGallery = null;
        }


        return $this;
    } // setGalleryId()

    /**
     * Set the value of [cover_photo_id] column.
     *
     * @param  int $v new value
     * @return Album The current object (for fluent API support)
     */
    public function setCoverPhotoId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->cover_photo_id !== $v) {
            $this->cover_photo_id = $v;
            $this->modifiedColumns[] = AlbumPeer::COVER_PHOTO_ID;
        }


        return $this;
    } // setCoverPhotoId()

    /**
     * Set the value of [rank] column.
     *
     * @param  int $v new value
     * @return Album The current object (for fluent API support)
     */
    public function setRank($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->rank !== $v) {
            $this->rank = $v;
            $this->modifiedColumns[] = AlbumPeer::RANK;
        }


        return $this;
    } // setRank()

    /**
     * Set the value of [is_public] column.
     *
     * @param  int $v new value
     * @return Album The current object (for fluent API support)
     */
    public function setIsPublic($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->is_public !== $v) {
            $this->is_public = $v;
            $this->modifiedColumns[] = AlbumPeer::IS_PUBLIC;
        }


        return $this;
    } // setIsPublic()

    /**
     * Set the value of [max_width] column.
     *
     * @param  int $v new value
     * @return Album The current object (for fluent API support)
     */
    public function setMaxWidth($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->max_width !== $v) {
            $this->max_width = $v;
            $this->modifiedColumns[] = AlbumPeer::MAX_WIDTH;
        }


        return $this;
    } // setMaxWidth()

    /**
     * Set the value of [max_height] column.
     *
     * @param  int $v new value
     * @return Album The current object (for fluent API support)
     */
    public function setMaxHeight($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->max_height !== $v) {
            $this->max_height = $v;
            $this->modifiedColumns[] = AlbumPeer::MAX_HEIGHT;
        }


        return $this;
    } // setMaxHeight()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->rank !== 1) {
                return false;
            }

            if ($this->is_public !== 0) {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->title = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->title_slug = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->description = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->gallery_id = ($row[$startcol + 4] !== null) ? (int) $row[$startcol + 4] : null;
            $this->cover_photo_id = ($row[$startcol + 5] !== null) ? (int) $row[$startcol + 5] : null;
            $this->rank = ($row[$startcol + 6] !== null) ? (int) $row[$startcol + 6] : null;
            $this->is_public = ($row[$startcol + 7] !== null) ? (int) $row[$startcol + 7] : null;
            $this->max_width = ($row[$startcol + 8] !== null) ? (int) $row[$startcol + 8] : null;
            $this->max_height = ($row[$startcol + 9] !== null) ? (int) $row[$startcol + 9] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 10; // 10 = AlbumPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating Album object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aGallery !== null && $this->gallery_id !== $this->aGallery->getId()) {
            $this->aGallery = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(AlbumPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = AlbumPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aGallery = null;
            $this->collPhotos = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(AlbumPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = AlbumQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(AlbumPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                AlbumPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aGallery !== null) {
                if ($this->aGallery->isModified() || $this->aGallery->isNew()) {
                    $affectedRows += $this->aGallery->save($con);
                }
                $this->setGallery($this->aGallery);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->photosScheduledForDeletion !== null) {
                if (!$this->photosScheduledForDeletion->isEmpty()) {
                    PhotoQuery::create()
                        ->filterByPrimaryKeys($this->photosScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->photosScheduledForDeletion = null;
                }
            }

            if ($this->collPhotos !== null) {
                foreach ($this->collPhotos as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = AlbumPeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . AlbumPeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(AlbumPeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`id`';
        }
        if ($this->isColumnModified(AlbumPeer::TITLE)) {
            $modifiedColumns[':p' . $index++]  = '`title`';
        }
        if ($this->isColumnModified(AlbumPeer::TITLE_SLUG)) {
            $modifiedColumns[':p' . $index++]  = '`title_slug`';
        }
        if ($this->isColumnModified(AlbumPeer::DESCRIPTION)) {
            $modifiedColumns[':p' . $index++]  = '`description`';
        }
        if ($this->isColumnModified(AlbumPeer::GALLERY_ID)) {
            $modifiedColumns[':p' . $index++]  = '`gallery_id`';
        }
        if ($this->isColumnModified(AlbumPeer::COVER_PHOTO_ID)) {
            $modifiedColumns[':p' . $index++]  = '`cover_photo_id`';
        }
        if ($this->isColumnModified(AlbumPeer::RANK)) {
            $modifiedColumns[':p' . $index++]  = '`rank`';
        }
        if ($this->isColumnModified(AlbumPeer::IS_PUBLIC)) {
            $modifiedColumns[':p' . $index++]  = '`is_public`';
        }
        if ($this->isColumnModified(AlbumPeer::MAX_WIDTH)) {
            $modifiedColumns[':p' . $index++]  = '`max_width`';
        }
        if ($this->isColumnModified(AlbumPeer::MAX_HEIGHT)) {
            $modifiedColumns[':p' . $index++]  = '`max_height`';
        }

        $sql = sprintf(
            'INSERT INTO `album` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id`':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`title`':
                        $stmt->bindValue($identifier, $this->title, PDO::PARAM_STR);
                        break;
                    case '`title_slug`':
                        $stmt->bindValue($identifier, $this->title_slug, PDO::PARAM_STR);
                        break;
                    case '`description`':
                        $stmt->bindValue($identifier, $this->description, PDO::PARAM_STR);
                        break;
                    case '`gallery_id`':
                        $stmt->bindValue($identifier, $this->gallery_id, PDO::PARAM_INT);
                        break;
                    case '`cover_photo_id`':
                        $stmt->bindValue($identifier, $this->cover_photo_id, PDO::PARAM_INT);
                        break;
                    case '`rank`':
                        $stmt->bindValue($identifier, $this->rank, PDO::PARAM_INT);
                        break;
                    case '`is_public`':
                        $stmt->bindValue($identifier, $this->is_public, PDO::PARAM_INT);
                        break;
                    case '`max_width`':
                        $stmt->bindValue($identifier, $this->max_width, PDO::PARAM_INT);
                        break;
                    case '`max_height`':
                        $stmt->bindValue($identifier, $this->max_height, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aGallery !== null) {
                if (!$this->aGallery->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aGallery->getValidationFailures());
                }
            }


            if (($retval = AlbumPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collPhotos !== null) {
                    foreach ($this->collPhotos as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = AlbumPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getTitle();
                break;
            case 2:
                return $this->getTitleSlug();
                break;
            case 3:
                return $this->getDescription();
                break;
            case 4:
                return $this->getGalleryId();
                break;
            case 5:
                return $this->getCoverPhotoId();
                break;
            case 6:
                return $this->getRank();
                break;
            case 7:
                return $this->getIsPublic();
                break;
            case 8:
                return $this->getMaxWidth();
                break;
            case 9:
                return $this->getMaxHeight();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['Album'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Album'][$this->getPrimaryKey()] = true;
        $keys = AlbumPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getTitle(),
            $keys[2] => $this->getTitleSlug(),
            $keys[3] => $this->getDescription(),
            $keys[4] => $this->getGalleryId(),
            $keys[5] => $this->getCoverPhotoId(),
            $keys[6] => $this->getRank(),
            $keys[7] => $this->getIsPublic(),
            $keys[8] => $this->getMaxWidth(),
            $keys[9] => $this->getMaxHeight(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach($virtualColumns as $key => $virtualColumn)
        {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aGallery) {
                $result['Gallery'] = $this->aGallery->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collPhotos) {
                $result['Photos'] = $this->collPhotos->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = AlbumPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setTitle($value);
                break;
            case 2:
                $this->setTitleSlug($value);
                break;
            case 3:
                $this->setDescription($value);
                break;
            case 4:
                $this->setGalleryId($value);
                break;
            case 5:
                $this->setCoverPhotoId($value);
                break;
            case 6:
                $this->setRank($value);
                break;
            case 7:
                $this->setIsPublic($value);
                break;
            case 8:
                $this->setMaxWidth($value);
                break;
            case 9:
                $this->setMaxHeight($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = AlbumPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setTitle($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setTitleSlug($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setDescription($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setGalleryId($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setCoverPhotoId($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setRank($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setIsPublic($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setMaxWidth($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setMaxHeight($arr[$keys[9]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(AlbumPeer::DATABASE_NAME);

        if ($this->isColumnModified(AlbumPeer::ID)) $criteria->add(AlbumPeer::ID, $this->id);
        if ($this->isColumnModified(AlbumPeer::TITLE)) $criteria->add(AlbumPeer::TITLE, $this->title);
        if ($this->isColumnModified(AlbumPeer::TITLE_SLUG)) $criteria->add(AlbumPeer::TITLE_SLUG, $this->title_slug);
        if ($this->isColumnModified(AlbumPeer::DESCRIPTION)) $criteria->add(AlbumPeer::DESCRIPTION, $this->description);
        if ($this->isColumnModified(AlbumPeer::GALLERY_ID)) $criteria->add(AlbumPeer::GALLERY_ID, $this->gallery_id);
        if ($this->isColumnModified(AlbumPeer::COVER_PHOTO_ID)) $criteria->add(AlbumPeer::COVER_PHOTO_ID, $this->cover_photo_id);
        if ($this->isColumnModified(AlbumPeer::RANK)) $criteria->add(AlbumPeer::RANK, $this->rank);
        if ($this->isColumnModified(AlbumPeer::IS_PUBLIC)) $criteria->add(AlbumPeer::IS_PUBLIC, $this->is_public);
        if ($this->isColumnModified(AlbumPeer::MAX_WIDTH)) $criteria->add(AlbumPeer::MAX_WIDTH, $this->max_width);
        if ($this->isColumnModified(AlbumPeer::MAX_HEIGHT)) $criteria->add(AlbumPeer::MAX_HEIGHT, $this->max_height);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(AlbumPeer::DATABASE_NAME);
        $criteria->add(AlbumPeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of Album (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setTitle($this->getTitle());
        $copyObj->setTitleSlug($this->getTitleSlug());
        $copyObj->setDescription($this->getDescription());
        $copyObj->setGalleryId($this->getGalleryId());
        $copyObj->setCoverPhotoId($this->getCoverPhotoId());
        $copyObj->setRank($this->getRank());
        $copyObj->setIsPublic($this->getIsPublic());
        $copyObj->setMaxWidth($this->getMaxWidth());
        $copyObj->setMaxHeight($this->getMaxHeight());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getPhotos() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPhoto($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return Album Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return AlbumPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new AlbumPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a Gallery object.
     *
     * @param                  Gallery $v
     * @return Album The current object (for fluent API support)
     * @throws PropelException
     */
    public function setGallery(Gallery $v = null)
    {
        if ($v === null) {
            $this->setGalleryId(NULL);
        } else {
            $this->setGalleryId($v->getId());
        }

        $this->aGallery = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Gallery object, it will not be re-added.
        if ($v !== null) {
            $v->addAlbum($this);
        }


        return $this;
    }


    /**
     * Get the associated Gallery object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Gallery The associated Gallery object.
     * @throws PropelException
     */
    public function getGallery(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aGallery === null && ($this->gallery_id !== null) && $doQuery) {
            $this->aGallery = GalleryQuery::create()->findPk($this->gallery_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aGallery->addAlbums($this);
             */
        }

        return $this->aGallery;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('Photo' == $relationName) {
            $this->initPhotos();
        }
    }

    /**
     * Clears out the collPhotos collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Album The current object (for fluent API support)
     * @see        addPhotos()
     */
    public function clearPhotos()
    {
        $this->collPhotos = null; // important to set this to null since that means it is uninitialized
        $this->collPhotosPartial = null;

        return $this;
    }

    /**
     * reset is the collPhotos collection loaded partially
     *
     * @return void
     */
    public function resetPartialPhotos($v = true)
    {
        $this->collPhotosPartial = $v;
    }

    /**
     * Initializes the collPhotos collection.
     *
     * By default this just sets the collPhotos collection to an empty array (like clearcollPhotos());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPhotos($overrideExisting = true)
    {
        if (null !== $this->collPhotos && !$overrideExisting) {
            return;
        }
        $this->collPhotos = new PropelObjectCollection();
        $this->collPhotos->setModel('Photo');
    }

    /**
     * Gets an array of Photo objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Album is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Photo[] List of Photo objects
     * @throws PropelException
     */
    public function getPhotos($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPhotosPartial && !$this->isNew();
        if (null === $this->collPhotos || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPhotos) {
                // return empty collection
                $this->initPhotos();
            } else {
                $collPhotos = PhotoQuery::create(null, $criteria)
                    ->filterByAlbum($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPhotosPartial && count($collPhotos)) {
                      $this->initPhotos(false);

                      foreach ($collPhotos as $obj) {
                        if (false == $this->collPhotos->contains($obj)) {
                          $this->collPhotos->append($obj);
                        }
                      }

                      $this->collPhotosPartial = true;
                    }

                    $collPhotos->getInternalIterator()->rewind();

                    return $collPhotos;
                }

                if ($partial && $this->collPhotos) {
                    foreach ($this->collPhotos as $obj) {
                        if ($obj->isNew()) {
                            $collPhotos[] = $obj;
                        }
                    }
                }

                $this->collPhotos = $collPhotos;
                $this->collPhotosPartial = false;
            }
        }

        return $this->collPhotos;
    }

    /**
     * Sets a collection of Photo objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $photos A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Album The current object (for fluent API support)
     */
    public function setPhotos(PropelCollection $photos, PropelPDO $con = null)
    {
        $photosToDelete = $this->getPhotos(new Criteria(), $con)->diff($photos);


        $this->photosScheduledForDeletion = $photosToDelete;

        foreach ($photosToDelete as $photoRemoved) {
            $photoRemoved->setAlbum(null);
        }

        $this->collPhotos = null;
        foreach ($photos as $photo) {
            $this->addPhoto($photo);
        }

        $this->collPhotos = $photos;
        $this->collPhotosPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Photo objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Photo objects.
     * @throws PropelException
     */
    public function countPhotos(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPhotosPartial && !$this->isNew();
        if (null === $this->collPhotos || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPhotos) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getPhotos());
            }
            $query = PhotoQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByAlbum($this)
                ->count($con);
        }

        return count($this->collPhotos);
    }

    /**
     * Method called to associate a Photo object to this object
     * through the Photo foreign key attribute.
     *
     * @param    Photo $l Photo
     * @return Album The current object (for fluent API support)
     */
    public function addPhoto(Photo $l)
    {
        if ($this->collPhotos === null) {
            $this->initPhotos();
            $this->collPhotosPartial = true;
        }

        if (!in_array($l, $this->collPhotos->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPhoto($l);

            if ($this->photosScheduledForDeletion and $this->photosScheduledForDeletion->contains($l)) {
                $this->photosScheduledForDeletion->remove($this->photosScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	Photo $photo The photo object to add.
     */
    protected function doAddPhoto($photo)
    {
        $this->collPhotos[]= $photo;
        $photo->setAlbum($this);
    }

    /**
     * @param	Photo $photo The photo object to remove.
     * @return Album The current object (for fluent API support)
     */
    public function removePhoto($photo)
    {
        if ($this->getPhotos()->contains($photo)) {
            $this->collPhotos->remove($this->collPhotos->search($photo));
            if (null === $this->photosScheduledForDeletion) {
                $this->photosScheduledForDeletion = clone $this->collPhotos;
                $this->photosScheduledForDeletion->clear();
            }
            $this->photosScheduledForDeletion[]= clone $photo;
            $photo->setAlbum(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Album is new, it will return
     * an empty collection; or if this Album has previously
     * been saved, it will retrieve related Photos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Album.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Photo[] List of Photo objects
     */
    public function getPhotosJoinFile($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PhotoQuery::create(null, $criteria);
        $query->joinWith('File', $join_behavior);

        return $this->getPhotos($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->title = null;
        $this->title_slug = null;
        $this->description = null;
        $this->gallery_id = null;
        $this->cover_photo_id = null;
        $this->rank = null;
        $this->is_public = null;
        $this->max_width = null;
        $this->max_height = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collPhotos) {
                foreach ($this->collPhotos as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aGallery instanceof Persistent) {
              $this->aGallery->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collPhotos instanceof PropelCollection) {
            $this->collPhotos->clearIterator();
        }
        $this->collPhotos = null;
        $this->aGallery = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(AlbumPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
