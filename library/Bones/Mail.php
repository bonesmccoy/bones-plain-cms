<?php
class Bones_Mail extends Zend_Mail {

    public function __construct() {
        parent::__construct();
        $config = Zend_Registry::get("config");

        $transport = @$config->bones->email->transport->type;

        if ($transport) {
            $tr_config = array(
                "ssl" => $config->bones->email->transport->ssl,
                "auth" => $config->bones->email->transport->auth,
                "port" =>  $config->bones->email->transport->port,
                "username" => $config->bones->email->transport->username,
                "password" => $config->bones->email->transport->password
            );

            $transport = new $transport('smtp.gmail.com', $tr_config);
            $this->setDefaultTransport($transport);
        }



    }


    public function get_html_body($template_name, array $params ,$modules = "default") {

        $html = new Zend_View();
        $html->setScriptPath(APPLICATION_PATH . "/modules/$modules/views/emails/");
        foreach ($params as $key => $value) {
            $html->assign($key, $value);
        }
        return $html->render($template_name . ".phtml");
    }

    public function send($transport = null){
        try {
            parent::send();
            return true;
        } catch (Exception $e) {
            $recipients = implode(",", $this->getRecipients());
            $subject = $this->getSubject();
            $sender = $this->getFrom();
            Bones_Log::logError("Error sending email To: {$recipients} , From: $sender, Subject: $subject;. {$e->getMessage()}");
            return false;
        }
    }

}