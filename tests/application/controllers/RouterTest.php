<?php

//require_once 'PHPUnit/Framework/TestCase.php';
//
class RouterTest extends BonesTestCase
{

    public function setUp()
    {
        parent::setUp();

    }

    public function tearDown()
    {
        parent::tearDown();
    }

    public function testDefaultRoute() {

        $this->dispatch("/");
        $this->assertController("index");
        $this->assertModule("default");

    }

    public function testAdminRouting(){

        $this->dispatch("/cpanel");
        $this->assertModule("admin");

        $this->dispatch("/cpanel/login");
        $this->assertModule("admin");
        $this->assertController("login");
    }


}

