<?php

/**
 * http://www.trirand.com/blog/jqgrid/jqgrid.html
 */
class Admin_AclController extends Bones_Controller_Admin {

    public function init() {
        parent::init();
        $this->view->roles = AclRoleQuery::create()->find();
        $this->view->acl = $this->_acl;
    }

    public function indexAction() {
        $permissions = AclPermissionQuery::Create()->addAscendingOrderByColumn(AclPermissionPeer::MODULE)->addAscendingOrderByColumn(AclPermissionPeer::ROLE_ID)->find();
        $this->view->permissions = $permissions;
        $this->view->resources_tree = $this->_acl->getResourcesTree();
    }

    public function rolesAction() {
        
    }

    public function getDataAction() {

        $page = $this->getRequest()->getParam('page');
        $limit = $this->getRequest()->getParam('rows');
        $sidx = $this->getRequest()->getParam('sidx');
        $sord = $this->getRequest()->getParam('sord');
        $result = array();
        $perm = AclPermissionQuery::Create()->find();


        $count = count($perm);
        if ($count > 0) {
            $total_pages = ceil($count / $limit);
        } else {
            $total_pages = 0;
        }
        if ($page > $total_pages)
            $page = $total_pages;

        $perm = AclPermissionQuery::Create()->limit($limit)->find()->toArray();
        $data = new stdClass();
        $data->page = $page;
        $data->total = $total_pages;
        $data->records = $count;
        foreach ($perm as $permission) {
            //var_dump(array_values($permission));
            $result[] = array('id' => $permission['Id'],
                'cell' => array_values($permission)
            );
        }
        $data->rows = $result;
        $this->_helper->json($data);
    }

    public function saveAction() {
        $this->_helper->layout->disableLayout();

        $postData = $this->getRequest()->getPost();
        $con = Propel::getConnection();
        $con->beginTransaction();
        try {
            AclPermissionQuery::create()->deleteAll();

            foreach ($postData['permission'] as $role_id => $permission) {

                foreach ($permission as $module => $resources) {
                    foreach ($resources as $resource => $actions) {
                        $acl_permission = new AclPermission();
                        $acl_permission->setRoleId($role_id);
                        $acl_permission->setModule($module);
                        $acl_permission->setResource($resource);
                        $allowed_actions = array_keys($actions);
                        $acl_permission->setActions(serialize($allowed_actions));
                        $acl_permission->setPermission(1);
                        $acl_permission->save();
                    }
                }
            }
            $con->commit();
            $cache = Bones_Cache::get("bones");
            $cache->remove("resources_tree");
        } catch (Exception $e) {
            $con->rollBack();
            Bones_Log::logError($e->getMessage());
            $this->setErrorMessage($e->getMessage());
        }



        $this->_redirect($this->view->url(array('action' => 'index')));
    }

    public function saveRolesAction() {

        $this->_helper->layout->disableLayout();
        $postData = $this->getRequest()->getPost();

        $action = current(array_keys($postData['submit']));
        $id = current(array_keys($postData['submit'][$action]));

        $role = AclRolePeer::retrieveByPK($id);
        if (!($role instanceof AclRole)) {

            $role = new AclRole();
        }

        switch ($action) {

            case 'del' : {
                    $role->delete();
                }
                break;
            case 'save': {

                    $role->setName($postData['name'][$id]);
                    $role->setParentRole($postData['parent_role'][$id]);
                    $role->save();
                }
                break;
        }
        $this->_redirect($this->view->url(array('action' => 'roles')));
    }




}

