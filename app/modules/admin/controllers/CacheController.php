<?php
class Admin_CacheController extends Bones_Controller_Admin
{

    public function indexAction()
    {
        $cache_template = Bones_Cache::getAll();
        $this->view->cache_templates = $cache_template;
    }

    public function flushAction(){

        $post_data = $this->getRequest()->getPost();
        if (!empty($post_data['templates'])) {
            $templates = $post_data['templates'];

            foreach ($templates as $template_name) {
                $cache = Bones_Cache::get($template_name);
                $cache->clean(Zend_Cache::CLEANING_MODE_ALL);
            }
            $this->setInfoMessage($this->t->translate("Tutta la cache svuotata"));
        } else  {
            $this->setErrorMessage($this->t->translate("Non hai selezionato nessun tipo di cache"));
        }
        $this->redirect(array("action"=>"index"));
    }





}

