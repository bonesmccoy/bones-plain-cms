<?php
require_once 'Zend/Test/PHPUnit/ControllerTestCase.php';

class BonesTestCase extends Zend_Test_PHPUnit_ControllerTestCase
{
    /**
     * @var Zend_Application
     */
    protected $application;

    public function setUp() {
        $this->bootstrap = array($this, 'appBootstrap');
        parent::setUp();
        $locale = new Zend_Locale("it_IT");
        $translator = new Zend_Translate(
            array(
                'adapter' => 'ini',
                'content' => APPLICATION_PATH . '/../resources/i18n/' . $locale->getLanguage() . '/translation.ini',
                'locale'  => $locale->getLanguage()
            )
        );

        Zend_Registry::set('locale', $locale);
        Zend_Registry::set('Zend_Translate', $translator);

    }

    public function TearDown(){}

    public function appBootstrap() {
        $config = Bones_Config::load(Bones_Config::TESTING);

        //print_r($config);die();
        $this->application = new Zend_Application(null, $config);
        Propel::init(APPLICATION_PATH . "/configs/bones-conf-testing.php");
        $this->application->bootstrap();

    }
}