<?php


/**
 * Base class that represents a row from the 'acl_role' table.
 *
 *
 *
 * @package    propel.generator.ORM.om
 */
abstract class BaseAclRole extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'AclRolePeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        AclRolePeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the name field.
     * @var        string
     */
    protected $name;

    /**
     * The value for the parent_role field.
     * @var        int
     */
    protected $parent_role;

    /**
     * The value for the is_front_end field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $is_front_end;

    /**
     * @var        AclRole
     */
    protected $aAclRoleRelatedByParentRole;

    /**
     * @var        PropelObjectCollection|AclPermission[] Collection to store aggregation of AclPermission objects.
     */
    protected $collAclPermissions;
    protected $collAclPermissionsPartial;

    /**
     * @var        PropelObjectCollection|AclRole[] Collection to store aggregation of AclRole objects.
     */
    protected $collAclRolesRelatedById;
    protected $collAclRolesRelatedByIdPartial;

    /**
     * @var        PropelObjectCollection|Admin[] Collection to store aggregation of Admin objects.
     */
    protected $collAdmins;
    protected $collAdminsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $aclPermissionsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $aclRolesRelatedByIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $adminsScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->is_front_end = 0;
    }

    /**
     * Initializes internal state of BaseAclRole object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [name] column value.
     *
     * @return string
     */
    public function getName()
    {

        return $this->name;
    }

    /**
     * Get the [parent_role] column value.
     *
     * @return int
     */
    public function getParentRole()
    {

        return $this->parent_role;
    }

    /**
     * Get the [is_front_end] column value.
     *
     * @return int
     */
    public function getIsFrontEnd()
    {

        return $this->is_front_end;
    }

    /**
     * Set the value of [id] column.
     *
     * @param  int $v new value
     * @return AclRole The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = AclRolePeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [name] column.
     *
     * @param  string $v new value
     * @return AclRole The current object (for fluent API support)
     */
    public function setName($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->name !== $v) {
            $this->name = $v;
            $this->modifiedColumns[] = AclRolePeer::NAME;
        }


        return $this;
    } // setName()

    /**
     * Set the value of [parent_role] column.
     *
     * @param  int $v new value
     * @return AclRole The current object (for fluent API support)
     */
    public function setParentRole($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->parent_role !== $v) {
            $this->parent_role = $v;
            $this->modifiedColumns[] = AclRolePeer::PARENT_ROLE;
        }

        if ($this->aAclRoleRelatedByParentRole !== null && $this->aAclRoleRelatedByParentRole->getId() !== $v) {
            $this->aAclRoleRelatedByParentRole = null;
        }


        return $this;
    } // setParentRole()

    /**
     * Set the value of [is_front_end] column.
     *
     * @param  int $v new value
     * @return AclRole The current object (for fluent API support)
     */
    public function setIsFrontEnd($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->is_front_end !== $v) {
            $this->is_front_end = $v;
            $this->modifiedColumns[] = AclRolePeer::IS_FRONT_END;
        }


        return $this;
    } // setIsFrontEnd()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->is_front_end !== 0) {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->name = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->parent_role = ($row[$startcol + 2] !== null) ? (int) $row[$startcol + 2] : null;
            $this->is_front_end = ($row[$startcol + 3] !== null) ? (int) $row[$startcol + 3] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 4; // 4 = AclRolePeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating AclRole object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aAclRoleRelatedByParentRole !== null && $this->parent_role !== $this->aAclRoleRelatedByParentRole->getId()) {
            $this->aAclRoleRelatedByParentRole = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(AclRolePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = AclRolePeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aAclRoleRelatedByParentRole = null;
            $this->collAclPermissions = null;

            $this->collAclRolesRelatedById = null;

            $this->collAdmins = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(AclRolePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = AclRoleQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(AclRolePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                AclRolePeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aAclRoleRelatedByParentRole !== null) {
                if ($this->aAclRoleRelatedByParentRole->isModified() || $this->aAclRoleRelatedByParentRole->isNew()) {
                    $affectedRows += $this->aAclRoleRelatedByParentRole->save($con);
                }
                $this->setAclRoleRelatedByParentRole($this->aAclRoleRelatedByParentRole);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->aclPermissionsScheduledForDeletion !== null) {
                if (!$this->aclPermissionsScheduledForDeletion->isEmpty()) {
                    foreach ($this->aclPermissionsScheduledForDeletion as $aclPermission) {
                        // need to save related object because we set the relation to null
                        $aclPermission->save($con);
                    }
                    $this->aclPermissionsScheduledForDeletion = null;
                }
            }

            if ($this->collAclPermissions !== null) {
                foreach ($this->collAclPermissions as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->aclRolesRelatedByIdScheduledForDeletion !== null) {
                if (!$this->aclRolesRelatedByIdScheduledForDeletion->isEmpty()) {
                    foreach ($this->aclRolesRelatedByIdScheduledForDeletion as $aclRoleRelatedById) {
                        // need to save related object because we set the relation to null
                        $aclRoleRelatedById->save($con);
                    }
                    $this->aclRolesRelatedByIdScheduledForDeletion = null;
                }
            }

            if ($this->collAclRolesRelatedById !== null) {
                foreach ($this->collAclRolesRelatedById as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->adminsScheduledForDeletion !== null) {
                if (!$this->adminsScheduledForDeletion->isEmpty()) {
                    foreach ($this->adminsScheduledForDeletion as $admin) {
                        // need to save related object because we set the relation to null
                        $admin->save($con);
                    }
                    $this->adminsScheduledForDeletion = null;
                }
            }

            if ($this->collAdmins !== null) {
                foreach ($this->collAdmins as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = AclRolePeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . AclRolePeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(AclRolePeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`id`';
        }
        if ($this->isColumnModified(AclRolePeer::NAME)) {
            $modifiedColumns[':p' . $index++]  = '`name`';
        }
        if ($this->isColumnModified(AclRolePeer::PARENT_ROLE)) {
            $modifiedColumns[':p' . $index++]  = '`parent_role`';
        }
        if ($this->isColumnModified(AclRolePeer::IS_FRONT_END)) {
            $modifiedColumns[':p' . $index++]  = '`is_front_end`';
        }

        $sql = sprintf(
            'INSERT INTO `acl_role` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id`':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`name`':
                        $stmt->bindValue($identifier, $this->name, PDO::PARAM_STR);
                        break;
                    case '`parent_role`':
                        $stmt->bindValue($identifier, $this->parent_role, PDO::PARAM_INT);
                        break;
                    case '`is_front_end`':
                        $stmt->bindValue($identifier, $this->is_front_end, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aAclRoleRelatedByParentRole !== null) {
                if (!$this->aAclRoleRelatedByParentRole->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aAclRoleRelatedByParentRole->getValidationFailures());
                }
            }


            if (($retval = AclRolePeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collAclPermissions !== null) {
                    foreach ($this->collAclPermissions as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collAclRolesRelatedById !== null) {
                    foreach ($this->collAclRolesRelatedById as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collAdmins !== null) {
                    foreach ($this->collAdmins as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = AclRolePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getName();
                break;
            case 2:
                return $this->getParentRole();
                break;
            case 3:
                return $this->getIsFrontEnd();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['AclRole'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['AclRole'][$this->getPrimaryKey()] = true;
        $keys = AclRolePeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getName(),
            $keys[2] => $this->getParentRole(),
            $keys[3] => $this->getIsFrontEnd(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach($virtualColumns as $key => $virtualColumn)
        {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aAclRoleRelatedByParentRole) {
                $result['AclRoleRelatedByParentRole'] = $this->aAclRoleRelatedByParentRole->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collAclPermissions) {
                $result['AclPermissions'] = $this->collAclPermissions->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collAclRolesRelatedById) {
                $result['AclRolesRelatedById'] = $this->collAclRolesRelatedById->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collAdmins) {
                $result['Admins'] = $this->collAdmins->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = AclRolePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setName($value);
                break;
            case 2:
                $this->setParentRole($value);
                break;
            case 3:
                $this->setIsFrontEnd($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = AclRolePeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setName($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setParentRole($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setIsFrontEnd($arr[$keys[3]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(AclRolePeer::DATABASE_NAME);

        if ($this->isColumnModified(AclRolePeer::ID)) $criteria->add(AclRolePeer::ID, $this->id);
        if ($this->isColumnModified(AclRolePeer::NAME)) $criteria->add(AclRolePeer::NAME, $this->name);
        if ($this->isColumnModified(AclRolePeer::PARENT_ROLE)) $criteria->add(AclRolePeer::PARENT_ROLE, $this->parent_role);
        if ($this->isColumnModified(AclRolePeer::IS_FRONT_END)) $criteria->add(AclRolePeer::IS_FRONT_END, $this->is_front_end);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(AclRolePeer::DATABASE_NAME);
        $criteria->add(AclRolePeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of AclRole (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setName($this->getName());
        $copyObj->setParentRole($this->getParentRole());
        $copyObj->setIsFrontEnd($this->getIsFrontEnd());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getAclPermissions() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addAclPermission($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getAclRolesRelatedById() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addAclRoleRelatedById($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getAdmins() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addAdmin($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return AclRole Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return AclRolePeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new AclRolePeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a AclRole object.
     *
     * @param                  AclRole $v
     * @return AclRole The current object (for fluent API support)
     * @throws PropelException
     */
    public function setAclRoleRelatedByParentRole(AclRole $v = null)
    {
        if ($v === null) {
            $this->setParentRole(NULL);
        } else {
            $this->setParentRole($v->getId());
        }

        $this->aAclRoleRelatedByParentRole = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the AclRole object, it will not be re-added.
        if ($v !== null) {
            $v->addAclRoleRelatedById($this);
        }


        return $this;
    }


    /**
     * Get the associated AclRole object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return AclRole The associated AclRole object.
     * @throws PropelException
     */
    public function getAclRoleRelatedByParentRole(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aAclRoleRelatedByParentRole === null && ($this->parent_role !== null) && $doQuery) {
            $this->aAclRoleRelatedByParentRole = AclRoleQuery::create()->findPk($this->parent_role, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aAclRoleRelatedByParentRole->addAclRolesRelatedById($this);
             */
        }

        return $this->aAclRoleRelatedByParentRole;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('AclPermission' == $relationName) {
            $this->initAclPermissions();
        }
        if ('AclRoleRelatedById' == $relationName) {
            $this->initAclRolesRelatedById();
        }
        if ('Admin' == $relationName) {
            $this->initAdmins();
        }
    }

    /**
     * Clears out the collAclPermissions collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return AclRole The current object (for fluent API support)
     * @see        addAclPermissions()
     */
    public function clearAclPermissions()
    {
        $this->collAclPermissions = null; // important to set this to null since that means it is uninitialized
        $this->collAclPermissionsPartial = null;

        return $this;
    }

    /**
     * reset is the collAclPermissions collection loaded partially
     *
     * @return void
     */
    public function resetPartialAclPermissions($v = true)
    {
        $this->collAclPermissionsPartial = $v;
    }

    /**
     * Initializes the collAclPermissions collection.
     *
     * By default this just sets the collAclPermissions collection to an empty array (like clearcollAclPermissions());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initAclPermissions($overrideExisting = true)
    {
        if (null !== $this->collAclPermissions && !$overrideExisting) {
            return;
        }
        $this->collAclPermissions = new PropelObjectCollection();
        $this->collAclPermissions->setModel('AclPermission');
    }

    /**
     * Gets an array of AclPermission objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this AclRole is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|AclPermission[] List of AclPermission objects
     * @throws PropelException
     */
    public function getAclPermissions($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collAclPermissionsPartial && !$this->isNew();
        if (null === $this->collAclPermissions || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collAclPermissions) {
                // return empty collection
                $this->initAclPermissions();
            } else {
                $collAclPermissions = AclPermissionQuery::create(null, $criteria)
                    ->filterByAclRole($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collAclPermissionsPartial && count($collAclPermissions)) {
                      $this->initAclPermissions(false);

                      foreach ($collAclPermissions as $obj) {
                        if (false == $this->collAclPermissions->contains($obj)) {
                          $this->collAclPermissions->append($obj);
                        }
                      }

                      $this->collAclPermissionsPartial = true;
                    }

                    $collAclPermissions->getInternalIterator()->rewind();

                    return $collAclPermissions;
                }

                if ($partial && $this->collAclPermissions) {
                    foreach ($this->collAclPermissions as $obj) {
                        if ($obj->isNew()) {
                            $collAclPermissions[] = $obj;
                        }
                    }
                }

                $this->collAclPermissions = $collAclPermissions;
                $this->collAclPermissionsPartial = false;
            }
        }

        return $this->collAclPermissions;
    }

    /**
     * Sets a collection of AclPermission objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $aclPermissions A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return AclRole The current object (for fluent API support)
     */
    public function setAclPermissions(PropelCollection $aclPermissions, PropelPDO $con = null)
    {
        $aclPermissionsToDelete = $this->getAclPermissions(new Criteria(), $con)->diff($aclPermissions);


        $this->aclPermissionsScheduledForDeletion = $aclPermissionsToDelete;

        foreach ($aclPermissionsToDelete as $aclPermissionRemoved) {
            $aclPermissionRemoved->setAclRole(null);
        }

        $this->collAclPermissions = null;
        foreach ($aclPermissions as $aclPermission) {
            $this->addAclPermission($aclPermission);
        }

        $this->collAclPermissions = $aclPermissions;
        $this->collAclPermissionsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related AclPermission objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related AclPermission objects.
     * @throws PropelException
     */
    public function countAclPermissions(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collAclPermissionsPartial && !$this->isNew();
        if (null === $this->collAclPermissions || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collAclPermissions) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getAclPermissions());
            }
            $query = AclPermissionQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByAclRole($this)
                ->count($con);
        }

        return count($this->collAclPermissions);
    }

    /**
     * Method called to associate a AclPermission object to this object
     * through the AclPermission foreign key attribute.
     *
     * @param    AclPermission $l AclPermission
     * @return AclRole The current object (for fluent API support)
     */
    public function addAclPermission(AclPermission $l)
    {
        if ($this->collAclPermissions === null) {
            $this->initAclPermissions();
            $this->collAclPermissionsPartial = true;
        }

        if (!in_array($l, $this->collAclPermissions->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddAclPermission($l);

            if ($this->aclPermissionsScheduledForDeletion and $this->aclPermissionsScheduledForDeletion->contains($l)) {
                $this->aclPermissionsScheduledForDeletion->remove($this->aclPermissionsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	AclPermission $aclPermission The aclPermission object to add.
     */
    protected function doAddAclPermission($aclPermission)
    {
        $this->collAclPermissions[]= $aclPermission;
        $aclPermission->setAclRole($this);
    }

    /**
     * @param	AclPermission $aclPermission The aclPermission object to remove.
     * @return AclRole The current object (for fluent API support)
     */
    public function removeAclPermission($aclPermission)
    {
        if ($this->getAclPermissions()->contains($aclPermission)) {
            $this->collAclPermissions->remove($this->collAclPermissions->search($aclPermission));
            if (null === $this->aclPermissionsScheduledForDeletion) {
                $this->aclPermissionsScheduledForDeletion = clone $this->collAclPermissions;
                $this->aclPermissionsScheduledForDeletion->clear();
            }
            $this->aclPermissionsScheduledForDeletion[]= $aclPermission;
            $aclPermission->setAclRole(null);
        }

        return $this;
    }

    /**
     * Clears out the collAclRolesRelatedById collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return AclRole The current object (for fluent API support)
     * @see        addAclRolesRelatedById()
     */
    public function clearAclRolesRelatedById()
    {
        $this->collAclRolesRelatedById = null; // important to set this to null since that means it is uninitialized
        $this->collAclRolesRelatedByIdPartial = null;

        return $this;
    }

    /**
     * reset is the collAclRolesRelatedById collection loaded partially
     *
     * @return void
     */
    public function resetPartialAclRolesRelatedById($v = true)
    {
        $this->collAclRolesRelatedByIdPartial = $v;
    }

    /**
     * Initializes the collAclRolesRelatedById collection.
     *
     * By default this just sets the collAclRolesRelatedById collection to an empty array (like clearcollAclRolesRelatedById());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initAclRolesRelatedById($overrideExisting = true)
    {
        if (null !== $this->collAclRolesRelatedById && !$overrideExisting) {
            return;
        }
        $this->collAclRolesRelatedById = new PropelObjectCollection();
        $this->collAclRolesRelatedById->setModel('AclRole');
    }

    /**
     * Gets an array of AclRole objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this AclRole is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|AclRole[] List of AclRole objects
     * @throws PropelException
     */
    public function getAclRolesRelatedById($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collAclRolesRelatedByIdPartial && !$this->isNew();
        if (null === $this->collAclRolesRelatedById || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collAclRolesRelatedById) {
                // return empty collection
                $this->initAclRolesRelatedById();
            } else {
                $collAclRolesRelatedById = AclRoleQuery::create(null, $criteria)
                    ->filterByAclRoleRelatedByParentRole($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collAclRolesRelatedByIdPartial && count($collAclRolesRelatedById)) {
                      $this->initAclRolesRelatedById(false);

                      foreach ($collAclRolesRelatedById as $obj) {
                        if (false == $this->collAclRolesRelatedById->contains($obj)) {
                          $this->collAclRolesRelatedById->append($obj);
                        }
                      }

                      $this->collAclRolesRelatedByIdPartial = true;
                    }

                    $collAclRolesRelatedById->getInternalIterator()->rewind();

                    return $collAclRolesRelatedById;
                }

                if ($partial && $this->collAclRolesRelatedById) {
                    foreach ($this->collAclRolesRelatedById as $obj) {
                        if ($obj->isNew()) {
                            $collAclRolesRelatedById[] = $obj;
                        }
                    }
                }

                $this->collAclRolesRelatedById = $collAclRolesRelatedById;
                $this->collAclRolesRelatedByIdPartial = false;
            }
        }

        return $this->collAclRolesRelatedById;
    }

    /**
     * Sets a collection of AclRoleRelatedById objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $aclRolesRelatedById A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return AclRole The current object (for fluent API support)
     */
    public function setAclRolesRelatedById(PropelCollection $aclRolesRelatedById, PropelPDO $con = null)
    {
        $aclRolesRelatedByIdToDelete = $this->getAclRolesRelatedById(new Criteria(), $con)->diff($aclRolesRelatedById);


        $this->aclRolesRelatedByIdScheduledForDeletion = $aclRolesRelatedByIdToDelete;

        foreach ($aclRolesRelatedByIdToDelete as $aclRoleRelatedByIdRemoved) {
            $aclRoleRelatedByIdRemoved->setAclRoleRelatedByParentRole(null);
        }

        $this->collAclRolesRelatedById = null;
        foreach ($aclRolesRelatedById as $aclRoleRelatedById) {
            $this->addAclRoleRelatedById($aclRoleRelatedById);
        }

        $this->collAclRolesRelatedById = $aclRolesRelatedById;
        $this->collAclRolesRelatedByIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related AclRole objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related AclRole objects.
     * @throws PropelException
     */
    public function countAclRolesRelatedById(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collAclRolesRelatedByIdPartial && !$this->isNew();
        if (null === $this->collAclRolesRelatedById || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collAclRolesRelatedById) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getAclRolesRelatedById());
            }
            $query = AclRoleQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByAclRoleRelatedByParentRole($this)
                ->count($con);
        }

        return count($this->collAclRolesRelatedById);
    }

    /**
     * Method called to associate a AclRole object to this object
     * through the AclRole foreign key attribute.
     *
     * @param    AclRole $l AclRole
     * @return AclRole The current object (for fluent API support)
     */
    public function addAclRoleRelatedById(AclRole $l)
    {
        if ($this->collAclRolesRelatedById === null) {
            $this->initAclRolesRelatedById();
            $this->collAclRolesRelatedByIdPartial = true;
        }

        if (!in_array($l, $this->collAclRolesRelatedById->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddAclRoleRelatedById($l);

            if ($this->aclRolesRelatedByIdScheduledForDeletion and $this->aclRolesRelatedByIdScheduledForDeletion->contains($l)) {
                $this->aclRolesRelatedByIdScheduledForDeletion->remove($this->aclRolesRelatedByIdScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	AclRoleRelatedById $aclRoleRelatedById The aclRoleRelatedById object to add.
     */
    protected function doAddAclRoleRelatedById($aclRoleRelatedById)
    {
        $this->collAclRolesRelatedById[]= $aclRoleRelatedById;
        $aclRoleRelatedById->setAclRoleRelatedByParentRole($this);
    }

    /**
     * @param	AclRoleRelatedById $aclRoleRelatedById The aclRoleRelatedById object to remove.
     * @return AclRole The current object (for fluent API support)
     */
    public function removeAclRoleRelatedById($aclRoleRelatedById)
    {
        if ($this->getAclRolesRelatedById()->contains($aclRoleRelatedById)) {
            $this->collAclRolesRelatedById->remove($this->collAclRolesRelatedById->search($aclRoleRelatedById));
            if (null === $this->aclRolesRelatedByIdScheduledForDeletion) {
                $this->aclRolesRelatedByIdScheduledForDeletion = clone $this->collAclRolesRelatedById;
                $this->aclRolesRelatedByIdScheduledForDeletion->clear();
            }
            $this->aclRolesRelatedByIdScheduledForDeletion[]= $aclRoleRelatedById;
            $aclRoleRelatedById->setAclRoleRelatedByParentRole(null);
        }

        return $this;
    }

    /**
     * Clears out the collAdmins collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return AclRole The current object (for fluent API support)
     * @see        addAdmins()
     */
    public function clearAdmins()
    {
        $this->collAdmins = null; // important to set this to null since that means it is uninitialized
        $this->collAdminsPartial = null;

        return $this;
    }

    /**
     * reset is the collAdmins collection loaded partially
     *
     * @return void
     */
    public function resetPartialAdmins($v = true)
    {
        $this->collAdminsPartial = $v;
    }

    /**
     * Initializes the collAdmins collection.
     *
     * By default this just sets the collAdmins collection to an empty array (like clearcollAdmins());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initAdmins($overrideExisting = true)
    {
        if (null !== $this->collAdmins && !$overrideExisting) {
            return;
        }
        $this->collAdmins = new PropelObjectCollection();
        $this->collAdmins->setModel('Admin');
    }

    /**
     * Gets an array of Admin objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this AclRole is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Admin[] List of Admin objects
     * @throws PropelException
     */
    public function getAdmins($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collAdminsPartial && !$this->isNew();
        if (null === $this->collAdmins || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collAdmins) {
                // return empty collection
                $this->initAdmins();
            } else {
                $collAdmins = AdminQuery::create(null, $criteria)
                    ->filterByAclRole($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collAdminsPartial && count($collAdmins)) {
                      $this->initAdmins(false);

                      foreach ($collAdmins as $obj) {
                        if (false == $this->collAdmins->contains($obj)) {
                          $this->collAdmins->append($obj);
                        }
                      }

                      $this->collAdminsPartial = true;
                    }

                    $collAdmins->getInternalIterator()->rewind();

                    return $collAdmins;
                }

                if ($partial && $this->collAdmins) {
                    foreach ($this->collAdmins as $obj) {
                        if ($obj->isNew()) {
                            $collAdmins[] = $obj;
                        }
                    }
                }

                $this->collAdmins = $collAdmins;
                $this->collAdminsPartial = false;
            }
        }

        return $this->collAdmins;
    }

    /**
     * Sets a collection of Admin objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $admins A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return AclRole The current object (for fluent API support)
     */
    public function setAdmins(PropelCollection $admins, PropelPDO $con = null)
    {
        $adminsToDelete = $this->getAdmins(new Criteria(), $con)->diff($admins);


        $this->adminsScheduledForDeletion = $adminsToDelete;

        foreach ($adminsToDelete as $adminRemoved) {
            $adminRemoved->setAclRole(null);
        }

        $this->collAdmins = null;
        foreach ($admins as $admin) {
            $this->addAdmin($admin);
        }

        $this->collAdmins = $admins;
        $this->collAdminsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Admin objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Admin objects.
     * @throws PropelException
     */
    public function countAdmins(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collAdminsPartial && !$this->isNew();
        if (null === $this->collAdmins || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collAdmins) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getAdmins());
            }
            $query = AdminQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByAclRole($this)
                ->count($con);
        }

        return count($this->collAdmins);
    }

    /**
     * Method called to associate a Admin object to this object
     * through the Admin foreign key attribute.
     *
     * @param    Admin $l Admin
     * @return AclRole The current object (for fluent API support)
     */
    public function addAdmin(Admin $l)
    {
        if ($this->collAdmins === null) {
            $this->initAdmins();
            $this->collAdminsPartial = true;
        }

        if (!in_array($l, $this->collAdmins->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddAdmin($l);

            if ($this->adminsScheduledForDeletion and $this->adminsScheduledForDeletion->contains($l)) {
                $this->adminsScheduledForDeletion->remove($this->adminsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	Admin $admin The admin object to add.
     */
    protected function doAddAdmin($admin)
    {
        $this->collAdmins[]= $admin;
        $admin->setAclRole($this);
    }

    /**
     * @param	Admin $admin The admin object to remove.
     * @return AclRole The current object (for fluent API support)
     */
    public function removeAdmin($admin)
    {
        if ($this->getAdmins()->contains($admin)) {
            $this->collAdmins->remove($this->collAdmins->search($admin));
            if (null === $this->adminsScheduledForDeletion) {
                $this->adminsScheduledForDeletion = clone $this->collAdmins;
                $this->adminsScheduledForDeletion->clear();
            }
            $this->adminsScheduledForDeletion[]= $admin;
            $admin->setAclRole(null);
        }

        return $this;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->name = null;
        $this->parent_role = null;
        $this->is_front_end = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collAclPermissions) {
                foreach ($this->collAclPermissions as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collAclRolesRelatedById) {
                foreach ($this->collAclRolesRelatedById as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collAdmins) {
                foreach ($this->collAdmins as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aAclRoleRelatedByParentRole instanceof Persistent) {
              $this->aAclRoleRelatedByParentRole->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collAclPermissions instanceof PropelCollection) {
            $this->collAclPermissions->clearIterator();
        }
        $this->collAclPermissions = null;
        if ($this->collAclRolesRelatedById instanceof PropelCollection) {
            $this->collAclRolesRelatedById->clearIterator();
        }
        $this->collAclRolesRelatedById = null;
        if ($this->collAdmins instanceof PropelCollection) {
            $this->collAdmins->clearIterator();
        }
        $this->collAdmins = null;
        $this->aAclRoleRelatedByParentRole = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(AclRolePeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
