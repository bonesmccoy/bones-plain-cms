<?php
require_once 'Zend/Config/Ini.php';

class Bones_Config {

    const WEB       = "web";
    const TESTING   = "testing";
    const CLI       = "cli";

    const BASE_CONFIG_PATH          = "/configs/bones/bones.ini";
    const APP_CONFIG_PATH           = "/configs/bones/app.ini";
    const TESTING_CONFIG_PATH       = "/configs/bones/testing.ini";


    const CLI_CONFIG_PATH           = "/configs/local/cli.ini";
    const WEB_CONFIG_PATH           = "/configs/local/web.ini";

    /**
     * @param $environment
     * @return Zend_Config_Ini
     * @throws Zend_Exception
     */
    public static function load($environment) {

        $base_config = new Zend_Config_Ini(
            APPLICATION_PATH . self::BASE_CONFIG_PATH,
            null,
            array('allowModifications' => true)
        );

        switch ($environment) {
            case self::WEB:
            case self::TESTING: {
                $app_config = new Zend_Config_Ini(
                    APPLICATION_PATH . self::APP_CONFIG_PATH,
                    null,
                    array('allowModifications' => true)
                );
                $base_config->merge($app_config);
             }
            break;
        }

        switch($environment) {
            case self::WEB: {
                $web_config_file = APPLICATION_PATH . self::WEB_CONFIG_PATH;
                if (!file_exists($web_config_file)) throw new Zend_Exception("Unable to find $environment configuration file");
                $web_config = new Zend_Config_Ini(
                    $web_config_file,
                    null,
                    array('allowModifications' => false)
                );
                $base_config->merge($web_config);
            }
            break;
            case self::CLI :{
                $cli_config_path = APPLICATION_PATH . self::CLI_CONFIG_PATH;
                if (!file_exists($cli_config_path)) throw new Zend_Exception("Unable to find $environment configuration file");
                $cli_config = new Zend_Config_Ini(
                    $cli_config_path,
                    null,
                    array('allowModifications' => false)
                );
                $base_config->merge($cli_config);

            }
            break;
            case self::TESTING: {
                $testing_config_path = APPLICATION_PATH . self::TESTING_CONFIG_PATH;
                if (!file_exists($testing_config_path)) throw new Zend_Exception("Unable to find $environment configuration file");
                $testing_config = new Zend_Config_Ini(
                    $testing_config_path,
                    null,
                    array('allowModifications' => false)
                );
                $base_config->merge($testing_config);
            }
            break;
            default : {
                throw new Zend_Exception("No allowed environment selected WEB|TESTING|CLI");
            }
            break;
        }
        return $base_config;
    }
}