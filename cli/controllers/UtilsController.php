<?php
class UtilsController extends Bones_Controller_Cli
{


    /**
     *	Just run
     *  php cli.php
     */
    public function encodePasswordAction () {
        $pwd = $this->getRequest()->getParam("pwd");
        if (empty($pwd) || !is_string($pwd)) {
            echo <<<USAGE
Usage:
    php cli.php utils encode-password pwd=value

USAGE;
            return;
        }

        echo "\n" . Admin::get_hashed_password($pwd) ."\n";

    }


    public static function info() {

        return <<<INFO
php bin/cli.php utils encode-password pwd=value


INFO;
    }


}