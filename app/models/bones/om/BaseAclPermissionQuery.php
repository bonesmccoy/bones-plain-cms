<?php


/**
 * Base class that represents a query for the 'acl_permission' table.
 *
 *
 *
 * @method AclPermissionQuery orderById($order = Criteria::ASC) Order by the id column
 * @method AclPermissionQuery orderByModule($order = Criteria::ASC) Order by the module column
 * @method AclPermissionQuery orderByRoleId($order = Criteria::ASC) Order by the role_id column
 * @method AclPermissionQuery orderByResource($order = Criteria::ASC) Order by the resource column
 * @method AclPermissionQuery orderByActions($order = Criteria::ASC) Order by the actions column
 * @method AclPermissionQuery orderByPermission($order = Criteria::ASC) Order by the permission column
 *
 * @method AclPermissionQuery groupById() Group by the id column
 * @method AclPermissionQuery groupByModule() Group by the module column
 * @method AclPermissionQuery groupByRoleId() Group by the role_id column
 * @method AclPermissionQuery groupByResource() Group by the resource column
 * @method AclPermissionQuery groupByActions() Group by the actions column
 * @method AclPermissionQuery groupByPermission() Group by the permission column
 *
 * @method AclPermissionQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method AclPermissionQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method AclPermissionQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method AclPermissionQuery leftJoinAclRole($relationAlias = null) Adds a LEFT JOIN clause to the query using the AclRole relation
 * @method AclPermissionQuery rightJoinAclRole($relationAlias = null) Adds a RIGHT JOIN clause to the query using the AclRole relation
 * @method AclPermissionQuery innerJoinAclRole($relationAlias = null) Adds a INNER JOIN clause to the query using the AclRole relation
 *
 * @method AclPermission findOne(PropelPDO $con = null) Return the first AclPermission matching the query
 * @method AclPermission findOneOrCreate(PropelPDO $con = null) Return the first AclPermission matching the query, or a new AclPermission object populated from the query conditions when no match is found
 *
 * @method AclPermission findOneByModule(string $module) Return the first AclPermission filtered by the module column
 * @method AclPermission findOneByRoleId(int $role_id) Return the first AclPermission filtered by the role_id column
 * @method AclPermission findOneByResource(string $resource) Return the first AclPermission filtered by the resource column
 * @method AclPermission findOneByActions(string $actions) Return the first AclPermission filtered by the actions column
 * @method AclPermission findOneByPermission(int $permission) Return the first AclPermission filtered by the permission column
 *
 * @method array findById(int $id) Return AclPermission objects filtered by the id column
 * @method array findByModule(string $module) Return AclPermission objects filtered by the module column
 * @method array findByRoleId(int $role_id) Return AclPermission objects filtered by the role_id column
 * @method array findByResource(string $resource) Return AclPermission objects filtered by the resource column
 * @method array findByActions(string $actions) Return AclPermission objects filtered by the actions column
 * @method array findByPermission(int $permission) Return AclPermission objects filtered by the permission column
 *
 * @package    propel.generator.ORM.om
 */
abstract class BaseAclPermissionQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseAclPermissionQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'ORM';
        }
        if (null === $modelName) {
            $modelName = 'AclPermission';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new AclPermissionQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   AclPermissionQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return AclPermissionQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof AclPermissionQuery) {
            return $criteria;
        }
        $query = new AclPermissionQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   AclPermission|AclPermission[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = AclPermissionPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(AclPermissionPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 AclPermission A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 AclPermission A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `module`, `role_id`, `resource`, `actions`, `permission` FROM `acl_permission` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new AclPermission();
            $obj->hydrate($row);
            AclPermissionPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return AclPermission|AclPermission[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|AclPermission[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return AclPermissionQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(AclPermissionPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return AclPermissionQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(AclPermissionPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AclPermissionQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(AclPermissionPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(AclPermissionPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AclPermissionPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the module column
     *
     * Example usage:
     * <code>
     * $query->filterByModule('fooValue');   // WHERE module = 'fooValue'
     * $query->filterByModule('%fooValue%'); // WHERE module LIKE '%fooValue%'
     * </code>
     *
     * @param     string $module The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AclPermissionQuery The current query, for fluid interface
     */
    public function filterByModule($module = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($module)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $module)) {
                $module = str_replace('*', '%', $module);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AclPermissionPeer::MODULE, $module, $comparison);
    }

    /**
     * Filter the query on the role_id column
     *
     * Example usage:
     * <code>
     * $query->filterByRoleId(1234); // WHERE role_id = 1234
     * $query->filterByRoleId(array(12, 34)); // WHERE role_id IN (12, 34)
     * $query->filterByRoleId(array('min' => 12)); // WHERE role_id >= 12
     * $query->filterByRoleId(array('max' => 12)); // WHERE role_id <= 12
     * </code>
     *
     * @see       filterByAclRole()
     *
     * @param     mixed $roleId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AclPermissionQuery The current query, for fluid interface
     */
    public function filterByRoleId($roleId = null, $comparison = null)
    {
        if (is_array($roleId)) {
            $useMinMax = false;
            if (isset($roleId['min'])) {
                $this->addUsingAlias(AclPermissionPeer::ROLE_ID, $roleId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($roleId['max'])) {
                $this->addUsingAlias(AclPermissionPeer::ROLE_ID, $roleId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AclPermissionPeer::ROLE_ID, $roleId, $comparison);
    }

    /**
     * Filter the query on the resource column
     *
     * Example usage:
     * <code>
     * $query->filterByResource('fooValue');   // WHERE resource = 'fooValue'
     * $query->filterByResource('%fooValue%'); // WHERE resource LIKE '%fooValue%'
     * </code>
     *
     * @param     string $resource The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AclPermissionQuery The current query, for fluid interface
     */
    public function filterByResource($resource = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($resource)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $resource)) {
                $resource = str_replace('*', '%', $resource);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AclPermissionPeer::RESOURCE, $resource, $comparison);
    }

    /**
     * Filter the query on the actions column
     *
     * Example usage:
     * <code>
     * $query->filterByActions('fooValue');   // WHERE actions = 'fooValue'
     * $query->filterByActions('%fooValue%'); // WHERE actions LIKE '%fooValue%'
     * </code>
     *
     * @param     string $actions The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AclPermissionQuery The current query, for fluid interface
     */
    public function filterByActions($actions = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($actions)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $actions)) {
                $actions = str_replace('*', '%', $actions);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AclPermissionPeer::ACTIONS, $actions, $comparison);
    }

    /**
     * Filter the query on the permission column
     *
     * Example usage:
     * <code>
     * $query->filterByPermission(1234); // WHERE permission = 1234
     * $query->filterByPermission(array(12, 34)); // WHERE permission IN (12, 34)
     * $query->filterByPermission(array('min' => 12)); // WHERE permission >= 12
     * $query->filterByPermission(array('max' => 12)); // WHERE permission <= 12
     * </code>
     *
     * @param     mixed $permission The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AclPermissionQuery The current query, for fluid interface
     */
    public function filterByPermission($permission = null, $comparison = null)
    {
        if (is_array($permission)) {
            $useMinMax = false;
            if (isset($permission['min'])) {
                $this->addUsingAlias(AclPermissionPeer::PERMISSION, $permission['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($permission['max'])) {
                $this->addUsingAlias(AclPermissionPeer::PERMISSION, $permission['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AclPermissionPeer::PERMISSION, $permission, $comparison);
    }

    /**
     * Filter the query by a related AclRole object
     *
     * @param   AclRole|PropelObjectCollection $aclRole The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 AclPermissionQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByAclRole($aclRole, $comparison = null)
    {
        if ($aclRole instanceof AclRole) {
            return $this
                ->addUsingAlias(AclPermissionPeer::ROLE_ID, $aclRole->getId(), $comparison);
        } elseif ($aclRole instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(AclPermissionPeer::ROLE_ID, $aclRole->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByAclRole() only accepts arguments of type AclRole or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the AclRole relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return AclPermissionQuery The current query, for fluid interface
     */
    public function joinAclRole($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('AclRole');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'AclRole');
        }

        return $this;
    }

    /**
     * Use the AclRole relation AclRole object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   AclRoleQuery A secondary query class using the current class as primary query
     */
    public function useAclRoleQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinAclRole($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'AclRole', 'AclRoleQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   AclPermission $aclPermission Object to remove from the list of results
     *
     * @return AclPermissionQuery The current query, for fluid interface
     */
    public function prune($aclPermission = null)
    {
        if ($aclPermission) {
            $this->addUsingAlias(AclPermissionPeer::ID, $aclPermission->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
