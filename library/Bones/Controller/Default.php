<?php

class Bones_Controller_Default extends Bones_Controller_Base {

    protected $_locale;
    protected $_language;
    protected $_facebookApp;
    const DESC_LIMIT = 30;
    const BAR_SHOWS = "SHOWS";
    const BAR_NEWS = "NEWS";
    const BAR_BANDCAMP = "BANDCAMP";
    const BAR_TWITTER = "TWITTER";
    const BAR_FACEBOOK = "FACEBOOK";

    public function init() {
        parent::init();
        
        $this->prepare_html_header();
        $this->view->controller_name = $this->getRequest()->getControllerName();
        $this->view->body_class = "generic";
        $this->view->docType('XHTML1_STRICT');

        $this->view->error_messages = $this->getErrorMessages();
        $this->view->info_messages = $this->getInfoMessages();
        //var_dump($this->_facebookApp->getApp());

        if ($this->_request->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
            $this->view->track_virtual_page = $this->view->partial("partial/analytics.phtml", array("virtual_page" => $this->view->url()));
            ; //"/". $this->_request->getControllerName() . "/" . $this->_request->getActionName();
        }
    }

    
    protected function prepare_html_header() {

        $this->view->headLink()->appendStylesheet('/css/bones/jquery-ui-1.10.3.custom.min.css');
        $this->view->headScript()->appendFile('/js/jquery-1.9.1.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/jquery-ui-1.10.3.custom.min.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/jquery.prettyPhoto.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/shadowbox.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/jquery.uniform.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/jquery.masonry.min.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/default.js', 'text/javascript');
        $this->view->headMeta()->appendName('keywords', '');
        $this->view->headLink()->appendStylesheet("http://fonts.googleapis.com/css?family=Alegreya:400italic,400,700&subset=latin-ext");
        $this->view->headLink()->appendStylesheet('/css/style.css');
        $this->view->headLink()->appendStylesheet('/css/prettyPhoto.css');
        $this->view->headLink()->appendStylesheet('/css/shadowbox/shadowbox.css');
        $this->view->page_title = ucfirst(strtolower(str_replace("Controller", "", get_class($this))));
        $this->set_meta_description();
    }

    protected function set_meta_description($content = "") {
        $meta_description  = "";
        if (is_array($content)) {
            foreach ($content as $sentence) {
                $sentence_stripped = trim($sentence);
                $meta_description .= "$sentence_stripped. ";
                if (str_word_count($meta_description) > self::DESC_LIMIT) break;
            }
        } else { $meta_description = $content;}
        $this->view->meta_description = $meta_description;
    }

   
}

