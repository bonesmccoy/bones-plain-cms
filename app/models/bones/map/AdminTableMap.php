<?php



/**
 * This class defines the structure of the 'admin' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.ORM.map
 */
class AdminTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'ORM.map.AdminTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('admin');
        $this->setPhpName('Admin');
        $this->setClassname('Admin');
        $this->setPackage('ORM');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('first_name', 'FirstName', 'VARCHAR', true, 50, null);
        $this->addColumn('last_name', 'LastName', 'VARCHAR', true, 50, null);
        $this->addColumn('username', 'Username', 'VARCHAR', true, 20, null);
        $this->addColumn('password', 'Password', 'VARCHAR', true, 20, null);
        $this->addColumn('email', 'Email', 'VARCHAR', true, 100, null);
        $this->addForeignKey('role', 'Role', 'INTEGER', 'acl_role', 'id', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('AclRole', 'AclRole', RelationMap::MANY_TO_ONE, array('role' => 'id', ), null, null);
    } // buildRelations()

} // AdminTableMap
