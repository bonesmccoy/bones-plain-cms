<?php

class IndexController extends Bones_Controller_Default {

    public function init() {
        parent::init();
        
        $this->view->body_class = "home";
        $this->view->is_home = true;
    }

    public function indexAction() {

    }

}

