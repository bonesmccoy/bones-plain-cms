<?php
// Define path to application directory
defined('APPLICATION_PATH')
|| define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../'));

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    get_include_path(),
    realpath(APPLICATION_PATH . '/models')
)));


/** Zend_Application */
require_once 'propel/runtime/lib/Propel.php';
require_once 'Zend/Application.php';
require_once 'Zend/Exception.php';
require_once 'Bones/Config.php';
require_once 'fckeditor/fckeditor.php';