<?php

class Bones_Controller_Base extends Zend_Controller_Action {

    protected $_locale;
    protected $_language;
    protected $_acl;
    protected $_errorNamespace;
    protected $config;
    protected $t;
    protected $cacheManager;
    protected $logger;

    public function init() {
        parent::init();

        $this->config = Zend_Registry::get('config');
        $this->_locale = Zend_Registry::get('locale');
        $this->_language = $this->_locale->getLanguage();
        $this->t = Zend_Registry::get('Zend_Translate');
        $this->view->lang = $this->_language;
        $this->view->t = $this->t;
        $this->_acl = Zend_Registry::get("acl");

    }

    protected function check_permissions($auth) {

        $resourceName = $this->_request->getModuleName() . ":" . $this->_request->getModuleName() . "_" . $this->_request->getControllerName();
        $role = is_null($auth->getRole()) ? 'guest' : $auth->getRole();
        /** Check if the controller/action can be accessed by the current user */
        if (!$this->_acl->isAllowed($role, $resourceName, $this->_request->getActionName())) {
            $this->_errorNamespace->error = true;
            $this->setErrorMessage('Azione non consentita');
            $this->redirect_to_error();
        }
    }

    protected function redirect_to_error() {
        $this->_errorNamespace = new Zend_Session_Namespace(__CLASS__);
        $this->_errorNamespace->error = true;
        $this->_redirect($this->view->url(array('controller' => 'error', 'action' => 'denied')));
    }

    protected function clear_error() {
        $this->_errorNamespace = new Zend_Session_Namespace(__CLASS__);
        Zend_Session_Namespace::resetSingleInstance(__CLASS__);
    }

    protected function setErrorMessage($message) {
        $errorNS = new Zend_Session_Namespace(strtoupper($this->_request->getModuleName()) . '_ERROR');
        $errorNS->messages[] = $message;
    }

    protected function getErrorMessages() {
        $errorNS = new Zend_Session_Namespace(strtoupper($this->_request->getModuleName()) . '_ERROR');
        $messages = $errorNS->messages;
        $errorNS->unsetAll();
        return $messages;
    }

    protected function setInfoMessage($message) {
        $infoNS = new Zend_Session_Namespace(strtoupper($this->_request->getModuleName()) . '_INFO');
        $infoNS->messages[] = $message;
    }

    protected function getInfoMessages() {
        $infoNS = new Zend_Session_Namespace(strtoupper($this->_request->getModuleName()) . '_INFO');
        $messages = $infoNS->messages;
        $infoNS->unsetAll();
        return $messages;
    }

    protected function redirect($array, $options = array()){
        $this->_redirect($this->view->url($array), $options);
        return;
    }

}

