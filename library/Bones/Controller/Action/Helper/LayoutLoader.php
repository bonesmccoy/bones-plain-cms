<?php
class Bones_Controller_Action_Helper_LayoutLoader extends Zend_Controller_Action_Helper_Abstract
{

    public function preDispatch()
    {
        $config = Zend_Registry::get("config");
        $module = $this->getRequest()->getModuleName();
        if (isset($config->$module->resources->layout->layout)) {
            $layoutScript = $config->$module->resources->layout->layout;
            $this->getActionController()->getHelper('layout')->setLayout($layoutScript);
        }
    }
    
}
