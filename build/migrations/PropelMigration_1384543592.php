<?php

/**
 * Data object containing the SQL and PHP code to migrate the database
 * up to version 1384543592.
 * Generated on 2013-11-15 20:26:32 by bones
 */
class PropelMigration_1384543592
{

    public function preUp($manager)
    {
        // add the pre-migration code here
    }

    public function postUp($manager)
    {
        // add the post-migration code here
    }

    public function preDown($manager)
    {
        // add the pre-migration code here
    }

    public function postDown($manager)
    {
        // add the post-migration code here
    }

    /**
     * Get the SQL statements for the Up migration
     *
     * @return array list of the SQL strings to execute for the Up migration
     *               the keys being the datasources
     */
    public function getUpSQL()
    {
        return array (
  'ORM' => '
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `event`;

CREATE TABLE `language`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `lang` VARCHAR(10),
    `locale` VARCHAR(10),
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
',
);
    }

    /**
     * Get the SQL statements for the Down migration
     *
     * @return array list of the SQL strings to execute for the Down migration
     *               the keys being the datasources
     */
    public function getDownSQL()
    {
        return array (
  'ORM' => '
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `language`;

CREATE TABLE `event`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `title` VARCHAR(255),
    `venue` VARCHAR(500),
    `address` TEXT,
    `created` DATETIME,
    `start_date` DATETIME,
    `end_date` DATETIME,
    `image_id` INTEGER,
    `is_public` TINYINT DEFAULT 0,
    `event_type` VARCHAR(255) NOT NULL,
    `facebook_id` VARCHAR(255) NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `fk_live_shows_image` (`image_id`)
) ENGINE=InnoDB;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
',
);
    }

}