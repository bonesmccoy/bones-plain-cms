
FCKConfig.ToolbarSets["limelight"] = [
	['Cut','Copy','Paste','PasteText','PasteWord','-','Undo','Redo','-','RemoveFormat'],
	['Link','Unlink'],
	['JustifyLeft','JustifyCenter','JustifyRight','JustifyFull'],
	['OrderedList','UnorderedList','-','Outdent','Indent'],
	['Source'],
	"/",
	['Image','Table','Rule','SpecialChar','YouTube'],
	['Style'],
	['TextColor','BGColor']
];

FCKConfig.ToolbarSets["journal_post"] = [
  	['Cut','Copy','Paste','PasteText','PasteWord','-','Undo','Redo','-','RemoveFormat'],
  	['Link','Unlink'],
  	['OrderedList','UnorderedList'],
  	['Image','Table','Rule','SpecialChar'],
  	['Style'],
  	['TextColor','BGColor']
  ];

FCKConfig.ToolbarSets["minimal"] = [
['Bold','Italic','Underline','StrikeThrough', '-','Link','Unlink'],['TextColor','BGColor'],
['JustifyLeft','JustifyCenter','JustifyRight','JustifyFull'],
['OrderedList','UnorderedList','-','Outdent','Indent'],
['Image','YouTube'],['Source','RemoveFormat']
];

FCKConfig.StylesXmlPath = '/js/fck_styles.xml' ;
FCKConfig.EditorAreaCSS = '/css/fck.css' ;
