<?php

class Bones_Cache {


    public static function get($template_name){
       $cm = Zend_Registry::get("cachemanager");
       return $cm->getCache($template_name);
    }

    public static function getAll() {
        $config = Bones_Parse_Ini::parse(APPLICATION_PATH . '/configs/bones/bones.ini');
        $bones_cache_templates = @$config["resources"]["cachemanager"];

        $app_config = Bones_Parse_Ini::parse(APPLICATION_PATH . '/configs/bones/app.ini');
        $app_cache_templates = @$app_config["resources"]["cachemanager"];

        $full_templates = array_merge($bones_cache_templates, $app_cache_templates);
        return array_keys($full_templates);


    }
}