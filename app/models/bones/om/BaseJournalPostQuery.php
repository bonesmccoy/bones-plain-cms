<?php


/**
 * Base class that represents a query for the 'journal_post' table.
 *
 *
 *
 * @method JournalPostQuery orderById($order = Criteria::ASC) Order by the id column
 * @method JournalPostQuery orderByTitle($order = Criteria::ASC) Order by the title column
 * @method JournalPostQuery orderByTitleSlug($order = Criteria::ASC) Order by the title_slug column
 * @method JournalPostQuery orderByTextAbstract($order = Criteria::ASC) Order by the text_abstract column
 * @method JournalPostQuery orderByContent($order = Criteria::ASC) Order by the content column
 * @method JournalPostQuery orderByTags($order = Criteria::ASC) Order by the tags column
 * @method JournalPostQuery orderByCreatorUserId($order = Criteria::ASC) Order by the creator_user_id column
 * @method JournalPostQuery orderByCreated($order = Criteria::ASC) Order by the created column
 * @method JournalPostQuery orderByEditorUserId($order = Criteria::ASC) Order by the editor_user_id column
 * @method JournalPostQuery orderByEdited($order = Criteria::ASC) Order by the edited column
 * @method JournalPostQuery orderByStartDate($order = Criteria::ASC) Order by the start_date column
 * @method JournalPostQuery orderByEndDate($order = Criteria::ASC) Order by the end_date column
 * @method JournalPostQuery orderByRank($order = Criteria::ASC) Order by the rank column
 * @method JournalPostQuery orderByIsPublic($order = Criteria::ASC) Order by the is_public column
 * @method JournalPostQuery orderByJournalId($order = Criteria::ASC) Order by the journal_id column
 * @method JournalPostQuery orderByFileId($order = Criteria::ASC) Order by the file_id column
 * @method JournalPostQuery orderByFileType($order = Criteria::ASC) Order by the file_type column
 *
 * @method JournalPostQuery groupById() Group by the id column
 * @method JournalPostQuery groupByTitle() Group by the title column
 * @method JournalPostQuery groupByTitleSlug() Group by the title_slug column
 * @method JournalPostQuery groupByTextAbstract() Group by the text_abstract column
 * @method JournalPostQuery groupByContent() Group by the content column
 * @method JournalPostQuery groupByTags() Group by the tags column
 * @method JournalPostQuery groupByCreatorUserId() Group by the creator_user_id column
 * @method JournalPostQuery groupByCreated() Group by the created column
 * @method JournalPostQuery groupByEditorUserId() Group by the editor_user_id column
 * @method JournalPostQuery groupByEdited() Group by the edited column
 * @method JournalPostQuery groupByStartDate() Group by the start_date column
 * @method JournalPostQuery groupByEndDate() Group by the end_date column
 * @method JournalPostQuery groupByRank() Group by the rank column
 * @method JournalPostQuery groupByIsPublic() Group by the is_public column
 * @method JournalPostQuery groupByJournalId() Group by the journal_id column
 * @method JournalPostQuery groupByFileId() Group by the file_id column
 * @method JournalPostQuery groupByFileType() Group by the file_type column
 *
 * @method JournalPostQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method JournalPostQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method JournalPostQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method JournalPostQuery leftJoinJournal($relationAlias = null) Adds a LEFT JOIN clause to the query using the Journal relation
 * @method JournalPostQuery rightJoinJournal($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Journal relation
 * @method JournalPostQuery innerJoinJournal($relationAlias = null) Adds a INNER JOIN clause to the query using the Journal relation
 *
 * @method JournalPostQuery leftJoinFile($relationAlias = null) Adds a LEFT JOIN clause to the query using the File relation
 * @method JournalPostQuery rightJoinFile($relationAlias = null) Adds a RIGHT JOIN clause to the query using the File relation
 * @method JournalPostQuery innerJoinFile($relationAlias = null) Adds a INNER JOIN clause to the query using the File relation
 *
 * @method JournalPost findOne(PropelPDO $con = null) Return the first JournalPost matching the query
 * @method JournalPost findOneOrCreate(PropelPDO $con = null) Return the first JournalPost matching the query, or a new JournalPost object populated from the query conditions when no match is found
 *
 * @method JournalPost findOneByTitle(string $title) Return the first JournalPost filtered by the title column
 * @method JournalPost findOneByTitleSlug(string $title_slug) Return the first JournalPost filtered by the title_slug column
 * @method JournalPost findOneByTextAbstract(string $text_abstract) Return the first JournalPost filtered by the text_abstract column
 * @method JournalPost findOneByContent(string $content) Return the first JournalPost filtered by the content column
 * @method JournalPost findOneByTags(string $tags) Return the first JournalPost filtered by the tags column
 * @method JournalPost findOneByCreatorUserId(int $creator_user_id) Return the first JournalPost filtered by the creator_user_id column
 * @method JournalPost findOneByCreated(string $created) Return the first JournalPost filtered by the created column
 * @method JournalPost findOneByEditorUserId(int $editor_user_id) Return the first JournalPost filtered by the editor_user_id column
 * @method JournalPost findOneByEdited(string $edited) Return the first JournalPost filtered by the edited column
 * @method JournalPost findOneByStartDate(string $start_date) Return the first JournalPost filtered by the start_date column
 * @method JournalPost findOneByEndDate(string $end_date) Return the first JournalPost filtered by the end_date column
 * @method JournalPost findOneByRank(int $rank) Return the first JournalPost filtered by the rank column
 * @method JournalPost findOneByIsPublic(int $is_public) Return the first JournalPost filtered by the is_public column
 * @method JournalPost findOneByJournalId(int $journal_id) Return the first JournalPost filtered by the journal_id column
 * @method JournalPost findOneByFileId(int $file_id) Return the first JournalPost filtered by the file_id column
 * @method JournalPost findOneByFileType(string $file_type) Return the first JournalPost filtered by the file_type column
 *
 * @method array findById(int $id) Return JournalPost objects filtered by the id column
 * @method array findByTitle(string $title) Return JournalPost objects filtered by the title column
 * @method array findByTitleSlug(string $title_slug) Return JournalPost objects filtered by the title_slug column
 * @method array findByTextAbstract(string $text_abstract) Return JournalPost objects filtered by the text_abstract column
 * @method array findByContent(string $content) Return JournalPost objects filtered by the content column
 * @method array findByTags(string $tags) Return JournalPost objects filtered by the tags column
 * @method array findByCreatorUserId(int $creator_user_id) Return JournalPost objects filtered by the creator_user_id column
 * @method array findByCreated(string $created) Return JournalPost objects filtered by the created column
 * @method array findByEditorUserId(int $editor_user_id) Return JournalPost objects filtered by the editor_user_id column
 * @method array findByEdited(string $edited) Return JournalPost objects filtered by the edited column
 * @method array findByStartDate(string $start_date) Return JournalPost objects filtered by the start_date column
 * @method array findByEndDate(string $end_date) Return JournalPost objects filtered by the end_date column
 * @method array findByRank(int $rank) Return JournalPost objects filtered by the rank column
 * @method array findByIsPublic(int $is_public) Return JournalPost objects filtered by the is_public column
 * @method array findByJournalId(int $journal_id) Return JournalPost objects filtered by the journal_id column
 * @method array findByFileId(int $file_id) Return JournalPost objects filtered by the file_id column
 * @method array findByFileType(string $file_type) Return JournalPost objects filtered by the file_type column
 *
 * @package    propel.generator.ORM.om
 */
abstract class BaseJournalPostQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseJournalPostQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'ORM';
        }
        if (null === $modelName) {
            $modelName = 'JournalPost';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new JournalPostQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   JournalPostQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return JournalPostQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof JournalPostQuery) {
            return $criteria;
        }
        $query = new JournalPostQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   JournalPost|JournalPost[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = JournalPostPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(JournalPostPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 JournalPost A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 JournalPost A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `title`, `title_slug`, `text_abstract`, `content`, `tags`, `creator_user_id`, `created`, `editor_user_id`, `edited`, `start_date`, `end_date`, `rank`, `is_public`, `journal_id`, `file_id`, `file_type` FROM `journal_post` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new JournalPost();
            $obj->hydrate($row);
            JournalPostPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return JournalPost|JournalPost[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|JournalPost[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return JournalPostQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(JournalPostPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return JournalPostQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(JournalPostPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JournalPostQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(JournalPostPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(JournalPostPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JournalPostPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the title column
     *
     * Example usage:
     * <code>
     * $query->filterByTitle('fooValue');   // WHERE title = 'fooValue'
     * $query->filterByTitle('%fooValue%'); // WHERE title LIKE '%fooValue%'
     * </code>
     *
     * @param     string $title The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JournalPostQuery The current query, for fluid interface
     */
    public function filterByTitle($title = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($title)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $title)) {
                $title = str_replace('*', '%', $title);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(JournalPostPeer::TITLE, $title, $comparison);
    }

    /**
     * Filter the query on the title_slug column
     *
     * Example usage:
     * <code>
     * $query->filterByTitleSlug('fooValue');   // WHERE title_slug = 'fooValue'
     * $query->filterByTitleSlug('%fooValue%'); // WHERE title_slug LIKE '%fooValue%'
     * </code>
     *
     * @param     string $titleSlug The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JournalPostQuery The current query, for fluid interface
     */
    public function filterByTitleSlug($titleSlug = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($titleSlug)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $titleSlug)) {
                $titleSlug = str_replace('*', '%', $titleSlug);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(JournalPostPeer::TITLE_SLUG, $titleSlug, $comparison);
    }

    /**
     * Filter the query on the text_abstract column
     *
     * Example usage:
     * <code>
     * $query->filterByTextAbstract('fooValue');   // WHERE text_abstract = 'fooValue'
     * $query->filterByTextAbstract('%fooValue%'); // WHERE text_abstract LIKE '%fooValue%'
     * </code>
     *
     * @param     string $textAbstract The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JournalPostQuery The current query, for fluid interface
     */
    public function filterByTextAbstract($textAbstract = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($textAbstract)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $textAbstract)) {
                $textAbstract = str_replace('*', '%', $textAbstract);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(JournalPostPeer::TEXT_ABSTRACT, $textAbstract, $comparison);
    }

    /**
     * Filter the query on the content column
     *
     * Example usage:
     * <code>
     * $query->filterByContent('fooValue');   // WHERE content = 'fooValue'
     * $query->filterByContent('%fooValue%'); // WHERE content LIKE '%fooValue%'
     * </code>
     *
     * @param     string $content The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JournalPostQuery The current query, for fluid interface
     */
    public function filterByContent($content = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($content)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $content)) {
                $content = str_replace('*', '%', $content);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(JournalPostPeer::CONTENT, $content, $comparison);
    }

    /**
     * Filter the query on the tags column
     *
     * Example usage:
     * <code>
     * $query->filterByTags('fooValue');   // WHERE tags = 'fooValue'
     * $query->filterByTags('%fooValue%'); // WHERE tags LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tags The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JournalPostQuery The current query, for fluid interface
     */
    public function filterByTags($tags = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tags)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tags)) {
                $tags = str_replace('*', '%', $tags);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(JournalPostPeer::TAGS, $tags, $comparison);
    }

    /**
     * Filter the query on the creator_user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatorUserId(1234); // WHERE creator_user_id = 1234
     * $query->filterByCreatorUserId(array(12, 34)); // WHERE creator_user_id IN (12, 34)
     * $query->filterByCreatorUserId(array('min' => 12)); // WHERE creator_user_id >= 12
     * $query->filterByCreatorUserId(array('max' => 12)); // WHERE creator_user_id <= 12
     * </code>
     *
     * @param     mixed $creatorUserId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JournalPostQuery The current query, for fluid interface
     */
    public function filterByCreatorUserId($creatorUserId = null, $comparison = null)
    {
        if (is_array($creatorUserId)) {
            $useMinMax = false;
            if (isset($creatorUserId['min'])) {
                $this->addUsingAlias(JournalPostPeer::CREATOR_USER_ID, $creatorUserId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($creatorUserId['max'])) {
                $this->addUsingAlias(JournalPostPeer::CREATOR_USER_ID, $creatorUserId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JournalPostPeer::CREATOR_USER_ID, $creatorUserId, $comparison);
    }

    /**
     * Filter the query on the created column
     *
     * Example usage:
     * <code>
     * $query->filterByCreated('2011-03-14'); // WHERE created = '2011-03-14'
     * $query->filterByCreated('now'); // WHERE created = '2011-03-14'
     * $query->filterByCreated(array('max' => 'yesterday')); // WHERE created < '2011-03-13'
     * </code>
     *
     * @param     mixed $created The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JournalPostQuery The current query, for fluid interface
     */
    public function filterByCreated($created = null, $comparison = null)
    {
        if (is_array($created)) {
            $useMinMax = false;
            if (isset($created['min'])) {
                $this->addUsingAlias(JournalPostPeer::CREATED, $created['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($created['max'])) {
                $this->addUsingAlias(JournalPostPeer::CREATED, $created['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JournalPostPeer::CREATED, $created, $comparison);
    }

    /**
     * Filter the query on the editor_user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByEditorUserId(1234); // WHERE editor_user_id = 1234
     * $query->filterByEditorUserId(array(12, 34)); // WHERE editor_user_id IN (12, 34)
     * $query->filterByEditorUserId(array('min' => 12)); // WHERE editor_user_id >= 12
     * $query->filterByEditorUserId(array('max' => 12)); // WHERE editor_user_id <= 12
     * </code>
     *
     * @param     mixed $editorUserId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JournalPostQuery The current query, for fluid interface
     */
    public function filterByEditorUserId($editorUserId = null, $comparison = null)
    {
        if (is_array($editorUserId)) {
            $useMinMax = false;
            if (isset($editorUserId['min'])) {
                $this->addUsingAlias(JournalPostPeer::EDITOR_USER_ID, $editorUserId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($editorUserId['max'])) {
                $this->addUsingAlias(JournalPostPeer::EDITOR_USER_ID, $editorUserId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JournalPostPeer::EDITOR_USER_ID, $editorUserId, $comparison);
    }

    /**
     * Filter the query on the edited column
     *
     * Example usage:
     * <code>
     * $query->filterByEdited('2011-03-14'); // WHERE edited = '2011-03-14'
     * $query->filterByEdited('now'); // WHERE edited = '2011-03-14'
     * $query->filterByEdited(array('max' => 'yesterday')); // WHERE edited < '2011-03-13'
     * </code>
     *
     * @param     mixed $edited The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JournalPostQuery The current query, for fluid interface
     */
    public function filterByEdited($edited = null, $comparison = null)
    {
        if (is_array($edited)) {
            $useMinMax = false;
            if (isset($edited['min'])) {
                $this->addUsingAlias(JournalPostPeer::EDITED, $edited['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($edited['max'])) {
                $this->addUsingAlias(JournalPostPeer::EDITED, $edited['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JournalPostPeer::EDITED, $edited, $comparison);
    }

    /**
     * Filter the query on the start_date column
     *
     * Example usage:
     * <code>
     * $query->filterByStartDate('2011-03-14'); // WHERE start_date = '2011-03-14'
     * $query->filterByStartDate('now'); // WHERE start_date = '2011-03-14'
     * $query->filterByStartDate(array('max' => 'yesterday')); // WHERE start_date < '2011-03-13'
     * </code>
     *
     * @param     mixed $startDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JournalPostQuery The current query, for fluid interface
     */
    public function filterByStartDate($startDate = null, $comparison = null)
    {
        if (is_array($startDate)) {
            $useMinMax = false;
            if (isset($startDate['min'])) {
                $this->addUsingAlias(JournalPostPeer::START_DATE, $startDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($startDate['max'])) {
                $this->addUsingAlias(JournalPostPeer::START_DATE, $startDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JournalPostPeer::START_DATE, $startDate, $comparison);
    }

    /**
     * Filter the query on the end_date column
     *
     * Example usage:
     * <code>
     * $query->filterByEndDate('2011-03-14'); // WHERE end_date = '2011-03-14'
     * $query->filterByEndDate('now'); // WHERE end_date = '2011-03-14'
     * $query->filterByEndDate(array('max' => 'yesterday')); // WHERE end_date < '2011-03-13'
     * </code>
     *
     * @param     mixed $endDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JournalPostQuery The current query, for fluid interface
     */
    public function filterByEndDate($endDate = null, $comparison = null)
    {
        if (is_array($endDate)) {
            $useMinMax = false;
            if (isset($endDate['min'])) {
                $this->addUsingAlias(JournalPostPeer::END_DATE, $endDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($endDate['max'])) {
                $this->addUsingAlias(JournalPostPeer::END_DATE, $endDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JournalPostPeer::END_DATE, $endDate, $comparison);
    }

    /**
     * Filter the query on the rank column
     *
     * Example usage:
     * <code>
     * $query->filterByRank(1234); // WHERE rank = 1234
     * $query->filterByRank(array(12, 34)); // WHERE rank IN (12, 34)
     * $query->filterByRank(array('min' => 12)); // WHERE rank >= 12
     * $query->filterByRank(array('max' => 12)); // WHERE rank <= 12
     * </code>
     *
     * @param     mixed $rank The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JournalPostQuery The current query, for fluid interface
     */
    public function filterByRank($rank = null, $comparison = null)
    {
        if (is_array($rank)) {
            $useMinMax = false;
            if (isset($rank['min'])) {
                $this->addUsingAlias(JournalPostPeer::RANK, $rank['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rank['max'])) {
                $this->addUsingAlias(JournalPostPeer::RANK, $rank['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JournalPostPeer::RANK, $rank, $comparison);
    }

    /**
     * Filter the query on the is_public column
     *
     * Example usage:
     * <code>
     * $query->filterByIsPublic(1234); // WHERE is_public = 1234
     * $query->filterByIsPublic(array(12, 34)); // WHERE is_public IN (12, 34)
     * $query->filterByIsPublic(array('min' => 12)); // WHERE is_public >= 12
     * $query->filterByIsPublic(array('max' => 12)); // WHERE is_public <= 12
     * </code>
     *
     * @param     mixed $isPublic The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JournalPostQuery The current query, for fluid interface
     */
    public function filterByIsPublic($isPublic = null, $comparison = null)
    {
        if (is_array($isPublic)) {
            $useMinMax = false;
            if (isset($isPublic['min'])) {
                $this->addUsingAlias(JournalPostPeer::IS_PUBLIC, $isPublic['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($isPublic['max'])) {
                $this->addUsingAlias(JournalPostPeer::IS_PUBLIC, $isPublic['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JournalPostPeer::IS_PUBLIC, $isPublic, $comparison);
    }

    /**
     * Filter the query on the journal_id column
     *
     * Example usage:
     * <code>
     * $query->filterByJournalId(1234); // WHERE journal_id = 1234
     * $query->filterByJournalId(array(12, 34)); // WHERE journal_id IN (12, 34)
     * $query->filterByJournalId(array('min' => 12)); // WHERE journal_id >= 12
     * $query->filterByJournalId(array('max' => 12)); // WHERE journal_id <= 12
     * </code>
     *
     * @see       filterByJournal()
     *
     * @param     mixed $journalId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JournalPostQuery The current query, for fluid interface
     */
    public function filterByJournalId($journalId = null, $comparison = null)
    {
        if (is_array($journalId)) {
            $useMinMax = false;
            if (isset($journalId['min'])) {
                $this->addUsingAlias(JournalPostPeer::JOURNAL_ID, $journalId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($journalId['max'])) {
                $this->addUsingAlias(JournalPostPeer::JOURNAL_ID, $journalId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JournalPostPeer::JOURNAL_ID, $journalId, $comparison);
    }

    /**
     * Filter the query on the file_id column
     *
     * Example usage:
     * <code>
     * $query->filterByFileId(1234); // WHERE file_id = 1234
     * $query->filterByFileId(array(12, 34)); // WHERE file_id IN (12, 34)
     * $query->filterByFileId(array('min' => 12)); // WHERE file_id >= 12
     * $query->filterByFileId(array('max' => 12)); // WHERE file_id <= 12
     * </code>
     *
     * @see       filterByFile()
     *
     * @param     mixed $fileId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JournalPostQuery The current query, for fluid interface
     */
    public function filterByFileId($fileId = null, $comparison = null)
    {
        if (is_array($fileId)) {
            $useMinMax = false;
            if (isset($fileId['min'])) {
                $this->addUsingAlias(JournalPostPeer::FILE_ID, $fileId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fileId['max'])) {
                $this->addUsingAlias(JournalPostPeer::FILE_ID, $fileId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JournalPostPeer::FILE_ID, $fileId, $comparison);
    }

    /**
     * Filter the query on the file_type column
     *
     * Example usage:
     * <code>
     * $query->filterByFileType('fooValue');   // WHERE file_type = 'fooValue'
     * $query->filterByFileType('%fooValue%'); // WHERE file_type LIKE '%fooValue%'
     * </code>
     *
     * @param     string $fileType The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JournalPostQuery The current query, for fluid interface
     */
    public function filterByFileType($fileType = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($fileType)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $fileType)) {
                $fileType = str_replace('*', '%', $fileType);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(JournalPostPeer::FILE_TYPE, $fileType, $comparison);
    }

    /**
     * Filter the query by a related Journal object
     *
     * @param   Journal|PropelObjectCollection $journal The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 JournalPostQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByJournal($journal, $comparison = null)
    {
        if ($journal instanceof Journal) {
            return $this
                ->addUsingAlias(JournalPostPeer::JOURNAL_ID, $journal->getId(), $comparison);
        } elseif ($journal instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(JournalPostPeer::JOURNAL_ID, $journal->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByJournal() only accepts arguments of type Journal or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Journal relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return JournalPostQuery The current query, for fluid interface
     */
    public function joinJournal($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Journal');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Journal');
        }

        return $this;
    }

    /**
     * Use the Journal relation Journal object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   JournalQuery A secondary query class using the current class as primary query
     */
    public function useJournalQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinJournal($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Journal', 'JournalQuery');
    }

    /**
     * Filter the query by a related File object
     *
     * @param   File|PropelObjectCollection $file The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 JournalPostQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByFile($file, $comparison = null)
    {
        if ($file instanceof File) {
            return $this
                ->addUsingAlias(JournalPostPeer::FILE_ID, $file->getId(), $comparison);
        } elseif ($file instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(JournalPostPeer::FILE_ID, $file->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByFile() only accepts arguments of type File or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the File relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return JournalPostQuery The current query, for fluid interface
     */
    public function joinFile($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('File');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'File');
        }

        return $this;
    }

    /**
     * Use the File relation File object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   FileQuery A secondary query class using the current class as primary query
     */
    public function useFileQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinFile($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'File', 'FileQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   JournalPost $journalPost Object to remove from the list of results
     *
     * @return JournalPostQuery The current query, for fluid interface
     */
    public function prune($journalPost = null)
    {
        if ($journalPost) {
            $this->addUsingAlias(JournalPostPeer::ID, $journalPost->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
