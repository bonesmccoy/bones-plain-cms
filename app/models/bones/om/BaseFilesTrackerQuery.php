<?php


/**
 * Base class that represents a query for the 'files_tracker' table.
 *
 *
 *
 * @method FilesTrackerQuery orderById($order = Criteria::ASC) Order by the id column
 * @method FilesTrackerQuery orderByRefererUrl($order = Criteria::ASC) Order by the referer_url column
 * @method FilesTrackerQuery orderByIpAddess($order = Criteria::ASC) Order by the ip_addess column
 * @method FilesTrackerQuery orderByFilename($order = Criteria::ASC) Order by the fileName column
 * @method FilesTrackerQuery orderByDownloadedTime($order = Criteria::ASC) Order by the downloaded_time column
 *
 * @method FilesTrackerQuery groupById() Group by the id column
 * @method FilesTrackerQuery groupByRefererUrl() Group by the referer_url column
 * @method FilesTrackerQuery groupByIpAddess() Group by the ip_addess column
 * @method FilesTrackerQuery groupByFilename() Group by the fileName column
 * @method FilesTrackerQuery groupByDownloadedTime() Group by the downloaded_time column
 *
 * @method FilesTrackerQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method FilesTrackerQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method FilesTrackerQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method FilesTracker findOne(PropelPDO $con = null) Return the first FilesTracker matching the query
 * @method FilesTracker findOneOrCreate(PropelPDO $con = null) Return the first FilesTracker matching the query, or a new FilesTracker object populated from the query conditions when no match is found
 *
 * @method FilesTracker findOneByRefererUrl(string $referer_url) Return the first FilesTracker filtered by the referer_url column
 * @method FilesTracker findOneByIpAddess(string $ip_addess) Return the first FilesTracker filtered by the ip_addess column
 * @method FilesTracker findOneByFilename(string $fileName) Return the first FilesTracker filtered by the fileName column
 * @method FilesTracker findOneByDownloadedTime(string $downloaded_time) Return the first FilesTracker filtered by the downloaded_time column
 *
 * @method array findById(int $id) Return FilesTracker objects filtered by the id column
 * @method array findByRefererUrl(string $referer_url) Return FilesTracker objects filtered by the referer_url column
 * @method array findByIpAddess(string $ip_addess) Return FilesTracker objects filtered by the ip_addess column
 * @method array findByFilename(string $fileName) Return FilesTracker objects filtered by the fileName column
 * @method array findByDownloadedTime(string $downloaded_time) Return FilesTracker objects filtered by the downloaded_time column
 *
 * @package    propel.generator.ORM.om
 */
abstract class BaseFilesTrackerQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseFilesTrackerQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'ORM';
        }
        if (null === $modelName) {
            $modelName = 'FilesTracker';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new FilesTrackerQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   FilesTrackerQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return FilesTrackerQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof FilesTrackerQuery) {
            return $criteria;
        }
        $query = new FilesTrackerQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   FilesTracker|FilesTracker[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = FilesTrackerPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(FilesTrackerPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 FilesTracker A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 FilesTracker A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `referer_url`, `ip_addess`, `fileName`, `downloaded_time` FROM `files_tracker` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new FilesTracker();
            $obj->hydrate($row);
            FilesTrackerPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return FilesTracker|FilesTracker[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|FilesTracker[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return FilesTrackerQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(FilesTrackerPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return FilesTrackerQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(FilesTrackerPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FilesTrackerQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(FilesTrackerPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(FilesTrackerPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FilesTrackerPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the referer_url column
     *
     * Example usage:
     * <code>
     * $query->filterByRefererUrl('fooValue');   // WHERE referer_url = 'fooValue'
     * $query->filterByRefererUrl('%fooValue%'); // WHERE referer_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $refererUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FilesTrackerQuery The current query, for fluid interface
     */
    public function filterByRefererUrl($refererUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($refererUrl)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $refererUrl)) {
                $refererUrl = str_replace('*', '%', $refererUrl);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FilesTrackerPeer::REFERER_URL, $refererUrl, $comparison);
    }

    /**
     * Filter the query on the ip_addess column
     *
     * Example usage:
     * <code>
     * $query->filterByIpAddess('fooValue');   // WHERE ip_addess = 'fooValue'
     * $query->filterByIpAddess('%fooValue%'); // WHERE ip_addess LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ipAddess The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FilesTrackerQuery The current query, for fluid interface
     */
    public function filterByIpAddess($ipAddess = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ipAddess)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $ipAddess)) {
                $ipAddess = str_replace('*', '%', $ipAddess);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FilesTrackerPeer::IP_ADDESS, $ipAddess, $comparison);
    }

    /**
     * Filter the query on the fileName column
     *
     * Example usage:
     * <code>
     * $query->filterByFilename('fooValue');   // WHERE fileName = 'fooValue'
     * $query->filterByFilename('%fooValue%'); // WHERE fileName LIKE '%fooValue%'
     * </code>
     *
     * @param     string $filename The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FilesTrackerQuery The current query, for fluid interface
     */
    public function filterByFilename($filename = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($filename)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $filename)) {
                $filename = str_replace('*', '%', $filename);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FilesTrackerPeer::FILENAME, $filename, $comparison);
    }

    /**
     * Filter the query on the downloaded_time column
     *
     * Example usage:
     * <code>
     * $query->filterByDownloadedTime('2011-03-14'); // WHERE downloaded_time = '2011-03-14'
     * $query->filterByDownloadedTime('now'); // WHERE downloaded_time = '2011-03-14'
     * $query->filterByDownloadedTime(array('max' => 'yesterday')); // WHERE downloaded_time < '2011-03-13'
     * </code>
     *
     * @param     mixed $downloadedTime The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FilesTrackerQuery The current query, for fluid interface
     */
    public function filterByDownloadedTime($downloadedTime = null, $comparison = null)
    {
        if (is_array($downloadedTime)) {
            $useMinMax = false;
            if (isset($downloadedTime['min'])) {
                $this->addUsingAlias(FilesTrackerPeer::DOWNLOADED_TIME, $downloadedTime['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($downloadedTime['max'])) {
                $this->addUsingAlias(FilesTrackerPeer::DOWNLOADED_TIME, $downloadedTime['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FilesTrackerPeer::DOWNLOADED_TIME, $downloadedTime, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   FilesTracker $filesTracker Object to remove from the list of results
     *
     * @return FilesTrackerQuery The current query, for fluid interface
     */
    public function prune($filesTracker = null)
    {
        if ($filesTracker) {
            $this->addUsingAlias(FilesTrackerPeer::ID, $filesTracker->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
