<?php


/**
 * Base class that represents a query for the 'journal' table.
 *
 *
 *
 * @method JournalQuery orderById($order = Criteria::ASC) Order by the id column
 * @method JournalQuery orderByTitle($order = Criteria::ASC) Order by the title column
 * @method JournalQuery orderByTitleSlug($order = Criteria::ASC) Order by the title_slug column
 * @method JournalQuery orderByTextAbstract($order = Criteria::ASC) Order by the text_abstract column
 * @method JournalQuery orderByShowTitle($order = Criteria::ASC) Order by the show_title column
 * @method JournalQuery orderByShowAbstract($order = Criteria::ASC) Order by the show_abstract column
 * @method JournalQuery orderByShowFulltext($order = Criteria::ASC) Order by the show_fulltext column
 * @method JournalQuery orderByShowFileUpload($order = Criteria::ASC) Order by the show_file_upload column
 * @method JournalQuery orderByPostOrderType($order = Criteria::ASC) Order by the post_order_type column
 *
 * @method JournalQuery groupById() Group by the id column
 * @method JournalQuery groupByTitle() Group by the title column
 * @method JournalQuery groupByTitleSlug() Group by the title_slug column
 * @method JournalQuery groupByTextAbstract() Group by the text_abstract column
 * @method JournalQuery groupByShowTitle() Group by the show_title column
 * @method JournalQuery groupByShowAbstract() Group by the show_abstract column
 * @method JournalQuery groupByShowFulltext() Group by the show_fulltext column
 * @method JournalQuery groupByShowFileUpload() Group by the show_file_upload column
 * @method JournalQuery groupByPostOrderType() Group by the post_order_type column
 *
 * @method JournalQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method JournalQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method JournalQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method JournalQuery leftJoinJournalPost($relationAlias = null) Adds a LEFT JOIN clause to the query using the JournalPost relation
 * @method JournalQuery rightJoinJournalPost($relationAlias = null) Adds a RIGHT JOIN clause to the query using the JournalPost relation
 * @method JournalQuery innerJoinJournalPost($relationAlias = null) Adds a INNER JOIN clause to the query using the JournalPost relation
 *
 * @method Journal findOne(PropelPDO $con = null) Return the first Journal matching the query
 * @method Journal findOneOrCreate(PropelPDO $con = null) Return the first Journal matching the query, or a new Journal object populated from the query conditions when no match is found
 *
 * @method Journal findOneByTitle(string $title) Return the first Journal filtered by the title column
 * @method Journal findOneByTitleSlug(string $title_slug) Return the first Journal filtered by the title_slug column
 * @method Journal findOneByTextAbstract(string $text_abstract) Return the first Journal filtered by the text_abstract column
 * @method Journal findOneByShowTitle(int $show_title) Return the first Journal filtered by the show_title column
 * @method Journal findOneByShowAbstract(int $show_abstract) Return the first Journal filtered by the show_abstract column
 * @method Journal findOneByShowFulltext(int $show_fulltext) Return the first Journal filtered by the show_fulltext column
 * @method Journal findOneByShowFileUpload(int $show_file_upload) Return the first Journal filtered by the show_file_upload column
 * @method Journal findOneByPostOrderType(string $post_order_type) Return the first Journal filtered by the post_order_type column
 *
 * @method array findById(int $id) Return Journal objects filtered by the id column
 * @method array findByTitle(string $title) Return Journal objects filtered by the title column
 * @method array findByTitleSlug(string $title_slug) Return Journal objects filtered by the title_slug column
 * @method array findByTextAbstract(string $text_abstract) Return Journal objects filtered by the text_abstract column
 * @method array findByShowTitle(int $show_title) Return Journal objects filtered by the show_title column
 * @method array findByShowAbstract(int $show_abstract) Return Journal objects filtered by the show_abstract column
 * @method array findByShowFulltext(int $show_fulltext) Return Journal objects filtered by the show_fulltext column
 * @method array findByShowFileUpload(int $show_file_upload) Return Journal objects filtered by the show_file_upload column
 * @method array findByPostOrderType(string $post_order_type) Return Journal objects filtered by the post_order_type column
 *
 * @package    propel.generator.ORM.om
 */
abstract class BaseJournalQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseJournalQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'ORM';
        }
        if (null === $modelName) {
            $modelName = 'Journal';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new JournalQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   JournalQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return JournalQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof JournalQuery) {
            return $criteria;
        }
        $query = new JournalQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Journal|Journal[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = JournalPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(JournalPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Journal A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Journal A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `title`, `title_slug`, `text_abstract`, `show_title`, `show_abstract`, `show_fulltext`, `show_file_upload`, `post_order_type` FROM `journal` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Journal();
            $obj->hydrate($row);
            JournalPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Journal|Journal[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Journal[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return JournalQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(JournalPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return JournalQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(JournalPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JournalQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(JournalPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(JournalPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JournalPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the title column
     *
     * Example usage:
     * <code>
     * $query->filterByTitle('fooValue');   // WHERE title = 'fooValue'
     * $query->filterByTitle('%fooValue%'); // WHERE title LIKE '%fooValue%'
     * </code>
     *
     * @param     string $title The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JournalQuery The current query, for fluid interface
     */
    public function filterByTitle($title = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($title)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $title)) {
                $title = str_replace('*', '%', $title);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(JournalPeer::TITLE, $title, $comparison);
    }

    /**
     * Filter the query on the title_slug column
     *
     * Example usage:
     * <code>
     * $query->filterByTitleSlug('fooValue');   // WHERE title_slug = 'fooValue'
     * $query->filterByTitleSlug('%fooValue%'); // WHERE title_slug LIKE '%fooValue%'
     * </code>
     *
     * @param     string $titleSlug The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JournalQuery The current query, for fluid interface
     */
    public function filterByTitleSlug($titleSlug = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($titleSlug)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $titleSlug)) {
                $titleSlug = str_replace('*', '%', $titleSlug);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(JournalPeer::TITLE_SLUG, $titleSlug, $comparison);
    }

    /**
     * Filter the query on the text_abstract column
     *
     * Example usage:
     * <code>
     * $query->filterByTextAbstract('fooValue');   // WHERE text_abstract = 'fooValue'
     * $query->filterByTextAbstract('%fooValue%'); // WHERE text_abstract LIKE '%fooValue%'
     * </code>
     *
     * @param     string $textAbstract The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JournalQuery The current query, for fluid interface
     */
    public function filterByTextAbstract($textAbstract = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($textAbstract)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $textAbstract)) {
                $textAbstract = str_replace('*', '%', $textAbstract);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(JournalPeer::TEXT_ABSTRACT, $textAbstract, $comparison);
    }

    /**
     * Filter the query on the show_title column
     *
     * Example usage:
     * <code>
     * $query->filterByShowTitle(1234); // WHERE show_title = 1234
     * $query->filterByShowTitle(array(12, 34)); // WHERE show_title IN (12, 34)
     * $query->filterByShowTitle(array('min' => 12)); // WHERE show_title >= 12
     * $query->filterByShowTitle(array('max' => 12)); // WHERE show_title <= 12
     * </code>
     *
     * @param     mixed $showTitle The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JournalQuery The current query, for fluid interface
     */
    public function filterByShowTitle($showTitle = null, $comparison = null)
    {
        if (is_array($showTitle)) {
            $useMinMax = false;
            if (isset($showTitle['min'])) {
                $this->addUsingAlias(JournalPeer::SHOW_TITLE, $showTitle['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($showTitle['max'])) {
                $this->addUsingAlias(JournalPeer::SHOW_TITLE, $showTitle['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JournalPeer::SHOW_TITLE, $showTitle, $comparison);
    }

    /**
     * Filter the query on the show_abstract column
     *
     * Example usage:
     * <code>
     * $query->filterByShowAbstract(1234); // WHERE show_abstract = 1234
     * $query->filterByShowAbstract(array(12, 34)); // WHERE show_abstract IN (12, 34)
     * $query->filterByShowAbstract(array('min' => 12)); // WHERE show_abstract >= 12
     * $query->filterByShowAbstract(array('max' => 12)); // WHERE show_abstract <= 12
     * </code>
     *
     * @param     mixed $showAbstract The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JournalQuery The current query, for fluid interface
     */
    public function filterByShowAbstract($showAbstract = null, $comparison = null)
    {
        if (is_array($showAbstract)) {
            $useMinMax = false;
            if (isset($showAbstract['min'])) {
                $this->addUsingAlias(JournalPeer::SHOW_ABSTRACT, $showAbstract['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($showAbstract['max'])) {
                $this->addUsingAlias(JournalPeer::SHOW_ABSTRACT, $showAbstract['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JournalPeer::SHOW_ABSTRACT, $showAbstract, $comparison);
    }

    /**
     * Filter the query on the show_fulltext column
     *
     * Example usage:
     * <code>
     * $query->filterByShowFulltext(1234); // WHERE show_fulltext = 1234
     * $query->filterByShowFulltext(array(12, 34)); // WHERE show_fulltext IN (12, 34)
     * $query->filterByShowFulltext(array('min' => 12)); // WHERE show_fulltext >= 12
     * $query->filterByShowFulltext(array('max' => 12)); // WHERE show_fulltext <= 12
     * </code>
     *
     * @param     mixed $showFulltext The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JournalQuery The current query, for fluid interface
     */
    public function filterByShowFulltext($showFulltext = null, $comparison = null)
    {
        if (is_array($showFulltext)) {
            $useMinMax = false;
            if (isset($showFulltext['min'])) {
                $this->addUsingAlias(JournalPeer::SHOW_FULLTEXT, $showFulltext['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($showFulltext['max'])) {
                $this->addUsingAlias(JournalPeer::SHOW_FULLTEXT, $showFulltext['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JournalPeer::SHOW_FULLTEXT, $showFulltext, $comparison);
    }

    /**
     * Filter the query on the show_file_upload column
     *
     * Example usage:
     * <code>
     * $query->filterByShowFileUpload(1234); // WHERE show_file_upload = 1234
     * $query->filterByShowFileUpload(array(12, 34)); // WHERE show_file_upload IN (12, 34)
     * $query->filterByShowFileUpload(array('min' => 12)); // WHERE show_file_upload >= 12
     * $query->filterByShowFileUpload(array('max' => 12)); // WHERE show_file_upload <= 12
     * </code>
     *
     * @param     mixed $showFileUpload The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JournalQuery The current query, for fluid interface
     */
    public function filterByShowFileUpload($showFileUpload = null, $comparison = null)
    {
        if (is_array($showFileUpload)) {
            $useMinMax = false;
            if (isset($showFileUpload['min'])) {
                $this->addUsingAlias(JournalPeer::SHOW_FILE_UPLOAD, $showFileUpload['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($showFileUpload['max'])) {
                $this->addUsingAlias(JournalPeer::SHOW_FILE_UPLOAD, $showFileUpload['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JournalPeer::SHOW_FILE_UPLOAD, $showFileUpload, $comparison);
    }

    /**
     * Filter the query on the post_order_type column
     *
     * Example usage:
     * <code>
     * $query->filterByPostOrderType('fooValue');   // WHERE post_order_type = 'fooValue'
     * $query->filterByPostOrderType('%fooValue%'); // WHERE post_order_type LIKE '%fooValue%'
     * </code>
     *
     * @param     string $postOrderType The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JournalQuery The current query, for fluid interface
     */
    public function filterByPostOrderType($postOrderType = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($postOrderType)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $postOrderType)) {
                $postOrderType = str_replace('*', '%', $postOrderType);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(JournalPeer::POST_ORDER_TYPE, $postOrderType, $comparison);
    }

    /**
     * Filter the query by a related JournalPost object
     *
     * @param   JournalPost|PropelObjectCollection $journalPost  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 JournalQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByJournalPost($journalPost, $comparison = null)
    {
        if ($journalPost instanceof JournalPost) {
            return $this
                ->addUsingAlias(JournalPeer::ID, $journalPost->getJournalId(), $comparison);
        } elseif ($journalPost instanceof PropelObjectCollection) {
            return $this
                ->useJournalPostQuery()
                ->filterByPrimaryKeys($journalPost->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByJournalPost() only accepts arguments of type JournalPost or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the JournalPost relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return JournalQuery The current query, for fluid interface
     */
    public function joinJournalPost($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('JournalPost');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'JournalPost');
        }

        return $this;
    }

    /**
     * Use the JournalPost relation JournalPost object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   JournalPostQuery A secondary query class using the current class as primary query
     */
    public function useJournalPostQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinJournalPost($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'JournalPost', 'JournalPostQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Journal $journal Object to remove from the list of results
     *
     * @return JournalQuery The current query, for fluid interface
     */
    public function prune($journal = null)
    {
        if ($journal) {
            $this->addUsingAlias(JournalPeer::ID, $journal->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
