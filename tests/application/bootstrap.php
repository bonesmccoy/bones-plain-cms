<?php
error_reporting(E_ALL | E_STRICT);

require_once dirname(__FILE__) . "/../../app/configs/init.php";

require_once 'propel/runtime/lib/Propel.php';
require_once 'Zend/Application.php';
require_once 'Zend/Config/Ini.php';
require_once 'Zend/Loader/Autoloader.php';
require_once 'PHPUnit/Framework/TestCase.php';
require_once 'BonesTestCase.php';
