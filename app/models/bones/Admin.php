<?php



/**
 * Skeleton subclass for representing a row from the 'admin' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.ORM
 */
class Admin extends BaseAdmin
{

    public static function doLogin($username, $password) {
        $hashed_password = self::get_hashed_password($password);
        return AdminQuery::create()->filterByUsername($username)->filterByPassword($hashed_password)->findOne();
    }

    public static function get_hashed_password($password) {
        $config = Zend_Registry::get("config");
        $salt = $config->bones->secure->salt;
        return md5($password . $salt);
    }
}
