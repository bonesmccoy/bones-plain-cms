<?php
class Bones_Controller_Plugin_Multilanguage extends Zend_Controller_Plugin_Abstract {

    public function preDispatch(Zend_Controller_Request_Abstract $request) {

        $language = $this->_request->getParam('lang');
        $lang = LanguageQuery::create()->findOneByLang($language);
        $config = Zend_Registry::get("config");

        $default_locale = isset($config->resources->locale->default) ? $config->resources->locale->default : "it_IT";

        $locale = ($lang instanceof Language) ? new Zend_Locale($lang->getLocale()) : new Zend_Locale($default_locale);

        $translator = new Zend_Translate(
            array(
                'adapter' => 'ini',
                'content' => APPLICATION_PATH . '/../resources/i18n/' . $locale->getLanguage() . '/translation.ini',
                'locale'  => $locale->getLanguage()
            )
        );

        Zend_Registry::set('locale', $locale);
        Zend_Registry::set('Zend_Translate', $translator);

        $this->_request->setParam('lang',$locale->getLanguage());
    }
}