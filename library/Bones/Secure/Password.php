<?php
class Bones_Secure_Password {


    public static function generate_random($length = 8) {

        $chars =  'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
        $str = '';
        $max = strlen($chars) - 1;
        for ($i=0; $i < $length; $i++) $str .= $chars[rand(0, $max)];
        return $str;
    }
}