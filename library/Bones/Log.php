<?php

class Bones_Log {

    const INFO  = "info";
    const WARN  = "warn";
    const ERR   = "err";
    const DEBUG = "debug";

    public static function logInfo($message) {
        Bones_Log::Log(Bones_Log::INFO, $message);
    }

    public static function logDebug($message) {
        Bones_Log::Log(Bones_Log::INFO, $message);
    }

    public static function logError($message){
        Bones_Log::Log(Bones_Log::ERR, $message);
    }


    public static function Log($type, $message) {

        $conf = Zend_Registry::get('config');
        switch(true){
            case isset($_SERVER['HTTP_HOST']) :{
                $logname = $_SERVER['HTTP_HOST'];
            }
            break;
            default: {
                $logname = $conf->bones->logger->log_name;
            }
            break;
        }

        $path = $conf->bones->logger->log_path . date("Ymd") . "_{$logname}.log";

        $logger = new Zend_Log();
        $writer = new Zend_Log_Writer_Stream($path);
        $logger->addWriter($writer);

        switch(1) {
            case is_object($message):
            case is_array($message): {
                $msg = var_export($message,1);
            }
            break;
            default: {
                $msg = $message;
            }
        }
        $logger->$type($msg);
    }
}