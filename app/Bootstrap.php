<?php
class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{

    protected function _initConfig()
    {
        $config = new Zend_Config($this->getOptions());
        Zend_Registry::set('config', $config);
        return $config;
    }

    protected function _initRoutes()
    {
        $this->bootstrap('frontController');
        $controller = Zend_Controller_Front::getInstance();
        $config = Zend_Registry::get('config');
        $router = $controller->getRouter();
        $router->addConfig($config, 'routes');
    }

    protected function _initAppAutoload()
    {
        $autoloader = new Zend_Application_Module_Autoloader(array(
            'namespace' => 'App',
            'basePath' => dirname(__FILE__),
        ));
        return $autoloader;
    }

    protected function _initLayoutHelper()
    {
        $this->bootstrap('frontController');
        $layout = Zend_Controller_Action_HelperBroker::addHelper(new Bones_Controller_Action_Helper_LayoutLoader());
    }


    protected function _initSession()
    {
        Zend_Session::start();
    }

    protected function _initLocale()
    {
        $front = Zend_Controller_Front::getInstance();
        $front->registerPlugin(new Bones_Controller_Plugin_Multilanguage());
    }

    public function _initCache()
    {
        $this->bootstrap('cachemanager');
        $cacheManager = $this->getResource('cachemanager');
        Zend_Registry::set("cachemanager", $cacheManager);

    }

    protected function _initAcl()
    {
        $acl = Bones_Acl_Acl::getInstance();
        Zend_Registry::set("acl", $acl);
    }

    protected function _initErrorHandler()
    {
        $this->bootstrap('frontController');
        $front = Zend_Controller_Front::getInstance();
        $request = new Zend_Controller_Request_Http();
        $router = $front->getRouter();
        $router->route($request);
        $module_name = $request->getModuleName();
        $module_name = (!empty($module_name)) ? $module_name : "default";
        $front->registerPlugin(new Zend_Controller_Plugin_ErrorHandler(array('module' => $module_name)));
    }


}


