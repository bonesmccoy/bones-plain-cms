<?php
define("OUTDIR", APPLICATION_PATH . "/../build/migrations/");

class MigrationController extends Bones_Controller_Cli
{


  public function generateAction() {

        $classname = "PropelMigration_" . time();
        $filename =  $classname . ".php";
        $content = self::getClassTemplate($classname);
        $handle = fopen(OUTDIR . "/" . $filename, "w") or die("cannot write to " . OUTDIR);
        fwrite($handle, $content);
        echo("created migration $filename on " . OUTDIR . "\n");
    }

    public function filenameAction() {
        echo "PropelMigration_" . time() . ".php\n";
    }

    public static function info() {

        return <<<INFO
php bin/cli.php migration generate
php bin/cli.php migration filename

INFO;
    }


    public static function getClassTemplate($classname){
       return <<<CONTENT
<?php
/**
* class $classname generate with cli application
*
*/

define("ROOT", realpath(dirname(__FILE__) . '/../../'));

set_include_path(implode(PATH_SEPARATOR, array(
                get_include_path(),
                realpath(ROOT . '/application/models/'))
));

include realpath(ROOT . '/application/configs/bones-conf.php');

class $classname {

 public function preUp(\$manager)
    {
        // add the pre-migration code here
         echo \$this->printDescription('PUT YOUR MIGRATION DESC HERE IF YA WANT');
    }

    public function postUp(\$manager)
    {
        // add the post-migration code here
    }

    public function preDown(\$manager)
    {
        // add the pre-migration code here
        echo \$this->printDescription('PUT YOUR MIGRATION DESC HERE IF YA WANT');
    }

    public function postDown(\$manager)
    {
        // add the post-migration code here
    }

    /**
     * Get the SQL statements for the Up migration
     *
     * @return array list of the SQL strings to execute for the Up migration
     *               the keys being the datasources
     */
    public function getUpSQL()
    {
        return array (
          'ORM' => '
          SET FOREIGN_KEY_CHECKS = 0;
          SET FOREIGN_KEY_CHECKS = 1;
        ',
        );
    }

    /**
     * Get the SQL statements for the Down migration
     *
     * @return array list of the SQL strings to execute for the Down migration
     *               the keys being the datasources
     */
    public function getDownSQL()
    {
        return array (
          'ORM' => '
          SET FOREIGN_KEY_CHECKS = 0;
          SET FOREIGN_KEY_CHECKS = 1;
        ',
        );
    }

    public function printDescription(\$message = 'PUT YOUR MIGRATION DESC HERE IF YA WANT'){
       return "\\n\\n#########################################\\n\$message\\n#########################################\\n\\n";
    }
}
CONTENT;
    }
}