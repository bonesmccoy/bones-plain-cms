<?php



/**
 * This class defines the structure of the 'journal_post' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.ORM.map
 */
class JournalPostTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'ORM.map.JournalPostTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('journal_post');
        $this->setPhpName('JournalPost');
        $this->setClassname('JournalPost');
        $this->setPackage('ORM');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('title', 'Title', 'VARCHAR', true, 1024, null);
        $this->addColumn('title_slug', 'TitleSlug', 'VARCHAR', true, 1024, null);
        $this->addColumn('text_abstract', 'TextAbstract', 'LONGVARCHAR', false, null, null);
        $this->addColumn('content', 'Content', 'LONGVARCHAR', false, null, null);
        $this->addColumn('tags', 'Tags', 'VARCHAR', false, 500, null);
        $this->addColumn('creator_user_id', 'CreatorUserId', 'INTEGER', false, null, null);
        $this->addColumn('created', 'Created', 'TIMESTAMP', false, null, null);
        $this->addColumn('editor_user_id', 'EditorUserId', 'INTEGER', false, null, null);
        $this->addColumn('edited', 'Edited', 'TIMESTAMP', false, null, null);
        $this->addColumn('start_date', 'StartDate', 'TIMESTAMP', false, null, null);
        $this->addColumn('end_date', 'EndDate', 'TIMESTAMP', false, null, null);
        $this->addColumn('rank', 'Rank', 'INTEGER', false, null, null);
        $this->addColumn('is_public', 'IsPublic', 'TINYINT', false, null, null);
        $this->addForeignKey('journal_id', 'JournalId', 'INTEGER', 'journal', 'id', true, null, null);
        $this->addForeignKey('file_id', 'FileId', 'INTEGER', 'file', 'id', false, null, null);
        $this->addColumn('file_type', 'FileType', 'VARCHAR', true, 10, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Journal', 'Journal', RelationMap::MANY_TO_ONE, array('journal_id' => 'id', ), null, null);
        $this->addRelation('File', 'File', RelationMap::MANY_TO_ONE, array('file_id' => 'id', ), null, null);
    } // buildRelations()

} // JournalPostTableMap
