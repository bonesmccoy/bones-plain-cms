<?php
class Bones_Files_Export
{

    private $_headers;
    private $_data;
    private $_raw_data;

    const FORMAT_CSV = "csv";
    const FORMAT_XLS = "xls";
    const FORMAT_XML = "xml";


    public function __construct(ModelCriteria $query, $headers)
    {
        $this->_headers = $headers;
        $this->_raw_data = $query->find();
        $this->buildData();
    }


    public function toExcel()
    {
        $csv = "";
        $this->toArray();
        foreach ($this->_data as $row) {
            $csv .= implode(";", $row) . "\n";
        }
        return $csv;
    }

    public function toCSV()
    {
        $csv = "";
        $this->toArray();
        foreach ($this->_data as $row) {
            $csv .= implode(",", $row) . "\n";
        }
        return $csv;
    }

    protected function buildHeaders()
    {
        $headers = array();
        foreach (array_values($this->_headers) as $column_name) {

            if (is_array($column_name)){

                foreach(array_values($column_name) as $cn) {
                    $headers[] = "\"$cn\"";
                }

            } else  $headers[] = "\"$column_name\"";
        }
        return $headers;
    }

    protected function buildRow($propel_row, $headers = null)
    {
        $row = array();
        $h = (is_array($headers)) ? $headers : $this->_headers;

        foreach ($h as $propel_name => $column_name) {
            $filter = new Zend_Filter_Word_UnderscoreToCamelCase();
            $method = "get" . $filter->filter($propel_name);
            if (!method_exists($propel_row, $method)) {
                Bones_Log::logError(__CLASS__ . ": method $method doesn't exist for object " . get_class($propel_row));
                continue;
            }
            $value = $propel_row->$method();
            if (($value instanceof BaseObject)) {
               $sub_row = $this->buildRow($value, $column_name);
               $row = array_merge($row, $sub_row);
            } else  $row[] = (method_exists($propel_row, $method)) ? utf8_decode("\"{$value}\"") : "\"\"";
        }
        return $row;
    }

    private function buildData()
    {
        $this->_data[] = $this->buildHeaders();
        foreach ($this->_raw_data as $propel_row) {
            $this->_data[] = $this->buildRow($propel_row);
        }
        return $this;
    }

    public function toArray()
    {
        return $this->_data;
    }


    private function toXml($rootElement = null, $array = null, $xml = null ){
        Bones_Log::logInfo("NOT YET IMPLEMENTED");
        return false;

        $_xml = $xml;
        $this->toArray();
        if ($_xml === null) {
            $_xml = new SimpleXMLElement($rootElement !== null ? $rootElement : '<root/>');
        }
        $a = (is_array($array)) ? $array : $this->_data;
        foreach ($a as $k => $v) {
            if (is_array($v)) { //nested array
               $this->toXml($k, $v, $_xml->addChild($k));
            } else {
                var_dump($k);die();
                $_xml->addChild($k, $v);
            }
        }

        return $_xml->asXML();
    }

    public function dump($filename_with_path, $format)
    {
        switch ($format) {
            case self::FORMAT_CSV :
            {
                $fp = fopen($filename_with_path, "w");
                $content = $this->toCSV();
                fwrite($fp, $content);
                fclose($fp);


            }
                break;
        }
    }

}