<?php



/**
 * This class defines the structure of the 'acl_role' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.ORM.map
 */
class AclRoleTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'ORM.map.AclRoleTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('acl_role');
        $this->setPhpName('AclRole');
        $this->setClassname('AclRole');
        $this->setPackage('ORM');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('name', 'Name', 'VARCHAR', true, 20, null);
        $this->addForeignKey('parent_role', 'ParentRole', 'INTEGER', 'acl_role', 'id', false, null, null);
        $this->addColumn('is_front_end', 'IsFrontEnd', 'TINYINT', false, null, 0);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('AclRoleRelatedByParentRole', 'AclRole', RelationMap::MANY_TO_ONE, array('parent_role' => 'id', ), null, null);
        $this->addRelation('AclPermission', 'AclPermission', RelationMap::ONE_TO_MANY, array('id' => 'role_id', ), null, null, 'AclPermissions');
        $this->addRelation('AclRoleRelatedById', 'AclRole', RelationMap::ONE_TO_MANY, array('id' => 'parent_role', ), null, null, 'AclRolesRelatedById');
        $this->addRelation('Admin', 'Admin', RelationMap::ONE_TO_MANY, array('id' => 'role', ), null, null, 'Admins');
    } // buildRelations()

} // AclRoleTableMap
