<?php



/**
 * Skeleton subclass for representing a row from the 'album' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.ORM
 */
class Album extends BaseAlbum
{
public function getCoverPhoto() {

    if ($this->getCoverPhotoId() > 0) {
        $photo = PhotoQuery::create()->findOneById($this->getCoverPhotoId());
    }
    return (@$photo instanceof Photo) ? $photo : new Photo();
}

    public function getCoverPhotoFullPath ($width = "") {
        if ($this->getCoverPhotoId() > 0) {
            $photo = PhotoQuery::create()->findOneById($this->getCoverPhotoId());
            return $photo->getImage()->getFullPath($width);
        }
        return "";
    }
}
