<?php
require_once dirname(__FILE__) . "/../app/configs/init.php";

try {
    error_reporting(E_ALL);
    $config = Bones_Config::load(Bones_Config::CLI);
    $application = new Zend_Application(
        null,
        $config
    );
    Propel::init(APPLICATION_PATH . "/configs/bones-conf.php");
    $application->bootstrap()->run();

} catch (Exception $e) {
    echo get_class($e) . ": " . $e->getMessage();
}


