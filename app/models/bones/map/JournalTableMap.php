<?php



/**
 * This class defines the structure of the 'journal' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.ORM.map
 */
class JournalTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'ORM.map.JournalTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('journal');
        $this->setPhpName('Journal');
        $this->setClassname('Journal');
        $this->setPackage('ORM');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('title', 'Title', 'VARCHAR', true, 1024, null);
        $this->addColumn('title_slug', 'TitleSlug', 'VARCHAR', true, 1024, null);
        $this->addColumn('text_abstract', 'TextAbstract', 'LONGVARCHAR', false, null, null);
        $this->addColumn('show_title', 'ShowTitle', 'TINYINT', false, null, 1);
        $this->addColumn('show_abstract', 'ShowAbstract', 'TINYINT', false, null, 1);
        $this->addColumn('show_fulltext', 'ShowFulltext', 'TINYINT', false, null, 1);
        $this->addColumn('show_file_upload', 'ShowFileUpload', 'TINYINT', false, null, 1);
        $this->addColumn('post_order_type', 'PostOrderType', 'VARCHAR', false, 50, 'rank');
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('JournalPost', 'JournalPost', RelationMap::ONE_TO_MANY, array('id' => 'journal_id', ), null, null, 'JournalPosts');
    } // buildRelations()

} // JournalTableMap
