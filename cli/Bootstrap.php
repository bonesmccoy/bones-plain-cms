<?php
class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{

    protected function _initConfig()
    {
        $config = new Zend_Config($this->getOptions());
        Zend_Registry::set('config', $config);
        return $config;
    }

	protected function _initRouter ()
	{
        $this->bootstrap ('frontcontroller');
		$this->getResource ('frontcontroller')
			->setResponse(new Zend_Controller_Response_Cli())
			->setRouter (new Bones_Router_Cli ())
			->setRequest (new Zend_Controller_Request_Simple ());
	}


	protected function _initError ()
	{
		$frontcontroller = $this->getResource ('frontcontroller');

		$error = new Zend_Controller_Plugin_ErrorHandler ();
		$error->setErrorHandlerController ('error');

		$frontcontroller->registerPlugin ($error, 100);

		return $error;
	}


}

