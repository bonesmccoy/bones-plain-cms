<?php


/**
 * Base class that represents a query for the 'album' table.
 *
 *
 *
 * @method AlbumQuery orderById($order = Criteria::ASC) Order by the id column
 * @method AlbumQuery orderByTitle($order = Criteria::ASC) Order by the title column
 * @method AlbumQuery orderByTitleSlug($order = Criteria::ASC) Order by the title_slug column
 * @method AlbumQuery orderByDescription($order = Criteria::ASC) Order by the description column
 * @method AlbumQuery orderByGalleryId($order = Criteria::ASC) Order by the gallery_id column
 * @method AlbumQuery orderByCoverPhotoId($order = Criteria::ASC) Order by the cover_photo_id column
 * @method AlbumQuery orderByRank($order = Criteria::ASC) Order by the rank column
 * @method AlbumQuery orderByIsPublic($order = Criteria::ASC) Order by the is_public column
 * @method AlbumQuery orderByMaxWidth($order = Criteria::ASC) Order by the max_width column
 * @method AlbumQuery orderByMaxHeight($order = Criteria::ASC) Order by the max_height column
 *
 * @method AlbumQuery groupById() Group by the id column
 * @method AlbumQuery groupByTitle() Group by the title column
 * @method AlbumQuery groupByTitleSlug() Group by the title_slug column
 * @method AlbumQuery groupByDescription() Group by the description column
 * @method AlbumQuery groupByGalleryId() Group by the gallery_id column
 * @method AlbumQuery groupByCoverPhotoId() Group by the cover_photo_id column
 * @method AlbumQuery groupByRank() Group by the rank column
 * @method AlbumQuery groupByIsPublic() Group by the is_public column
 * @method AlbumQuery groupByMaxWidth() Group by the max_width column
 * @method AlbumQuery groupByMaxHeight() Group by the max_height column
 *
 * @method AlbumQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method AlbumQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method AlbumQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method AlbumQuery leftJoinGallery($relationAlias = null) Adds a LEFT JOIN clause to the query using the Gallery relation
 * @method AlbumQuery rightJoinGallery($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Gallery relation
 * @method AlbumQuery innerJoinGallery($relationAlias = null) Adds a INNER JOIN clause to the query using the Gallery relation
 *
 * @method AlbumQuery leftJoinPhoto($relationAlias = null) Adds a LEFT JOIN clause to the query using the Photo relation
 * @method AlbumQuery rightJoinPhoto($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Photo relation
 * @method AlbumQuery innerJoinPhoto($relationAlias = null) Adds a INNER JOIN clause to the query using the Photo relation
 *
 * @method Album findOne(PropelPDO $con = null) Return the first Album matching the query
 * @method Album findOneOrCreate(PropelPDO $con = null) Return the first Album matching the query, or a new Album object populated from the query conditions when no match is found
 *
 * @method Album findOneByTitle(string $title) Return the first Album filtered by the title column
 * @method Album findOneByTitleSlug(string $title_slug) Return the first Album filtered by the title_slug column
 * @method Album findOneByDescription(string $description) Return the first Album filtered by the description column
 * @method Album findOneByGalleryId(int $gallery_id) Return the first Album filtered by the gallery_id column
 * @method Album findOneByCoverPhotoId(int $cover_photo_id) Return the first Album filtered by the cover_photo_id column
 * @method Album findOneByRank(int $rank) Return the first Album filtered by the rank column
 * @method Album findOneByIsPublic(int $is_public) Return the first Album filtered by the is_public column
 * @method Album findOneByMaxWidth(int $max_width) Return the first Album filtered by the max_width column
 * @method Album findOneByMaxHeight(int $max_height) Return the first Album filtered by the max_height column
 *
 * @method array findById(int $id) Return Album objects filtered by the id column
 * @method array findByTitle(string $title) Return Album objects filtered by the title column
 * @method array findByTitleSlug(string $title_slug) Return Album objects filtered by the title_slug column
 * @method array findByDescription(string $description) Return Album objects filtered by the description column
 * @method array findByGalleryId(int $gallery_id) Return Album objects filtered by the gallery_id column
 * @method array findByCoverPhotoId(int $cover_photo_id) Return Album objects filtered by the cover_photo_id column
 * @method array findByRank(int $rank) Return Album objects filtered by the rank column
 * @method array findByIsPublic(int $is_public) Return Album objects filtered by the is_public column
 * @method array findByMaxWidth(int $max_width) Return Album objects filtered by the max_width column
 * @method array findByMaxHeight(int $max_height) Return Album objects filtered by the max_height column
 *
 * @package    propel.generator.ORM.om
 */
abstract class BaseAlbumQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseAlbumQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'ORM';
        }
        if (null === $modelName) {
            $modelName = 'Album';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new AlbumQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   AlbumQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return AlbumQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof AlbumQuery) {
            return $criteria;
        }
        $query = new AlbumQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Album|Album[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = AlbumPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(AlbumPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Album A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Album A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `title`, `title_slug`, `description`, `gallery_id`, `cover_photo_id`, `rank`, `is_public`, `max_width`, `max_height` FROM `album` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Album();
            $obj->hydrate($row);
            AlbumPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Album|Album[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Album[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return AlbumQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(AlbumPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return AlbumQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(AlbumPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AlbumQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(AlbumPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(AlbumPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AlbumPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the title column
     *
     * Example usage:
     * <code>
     * $query->filterByTitle('fooValue');   // WHERE title = 'fooValue'
     * $query->filterByTitle('%fooValue%'); // WHERE title LIKE '%fooValue%'
     * </code>
     *
     * @param     string $title The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AlbumQuery The current query, for fluid interface
     */
    public function filterByTitle($title = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($title)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $title)) {
                $title = str_replace('*', '%', $title);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AlbumPeer::TITLE, $title, $comparison);
    }

    /**
     * Filter the query on the title_slug column
     *
     * Example usage:
     * <code>
     * $query->filterByTitleSlug('fooValue');   // WHERE title_slug = 'fooValue'
     * $query->filterByTitleSlug('%fooValue%'); // WHERE title_slug LIKE '%fooValue%'
     * </code>
     *
     * @param     string $titleSlug The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AlbumQuery The current query, for fluid interface
     */
    public function filterByTitleSlug($titleSlug = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($titleSlug)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $titleSlug)) {
                $titleSlug = str_replace('*', '%', $titleSlug);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AlbumPeer::TITLE_SLUG, $titleSlug, $comparison);
    }

    /**
     * Filter the query on the description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE description = 'fooValue'
     * $query->filterByDescription('%fooValue%'); // WHERE description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AlbumQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $description)) {
                $description = str_replace('*', '%', $description);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AlbumPeer::DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the gallery_id column
     *
     * Example usage:
     * <code>
     * $query->filterByGalleryId(1234); // WHERE gallery_id = 1234
     * $query->filterByGalleryId(array(12, 34)); // WHERE gallery_id IN (12, 34)
     * $query->filterByGalleryId(array('min' => 12)); // WHERE gallery_id >= 12
     * $query->filterByGalleryId(array('max' => 12)); // WHERE gallery_id <= 12
     * </code>
     *
     * @see       filterByGallery()
     *
     * @param     mixed $galleryId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AlbumQuery The current query, for fluid interface
     */
    public function filterByGalleryId($galleryId = null, $comparison = null)
    {
        if (is_array($galleryId)) {
            $useMinMax = false;
            if (isset($galleryId['min'])) {
                $this->addUsingAlias(AlbumPeer::GALLERY_ID, $galleryId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($galleryId['max'])) {
                $this->addUsingAlias(AlbumPeer::GALLERY_ID, $galleryId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AlbumPeer::GALLERY_ID, $galleryId, $comparison);
    }

    /**
     * Filter the query on the cover_photo_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCoverPhotoId(1234); // WHERE cover_photo_id = 1234
     * $query->filterByCoverPhotoId(array(12, 34)); // WHERE cover_photo_id IN (12, 34)
     * $query->filterByCoverPhotoId(array('min' => 12)); // WHERE cover_photo_id >= 12
     * $query->filterByCoverPhotoId(array('max' => 12)); // WHERE cover_photo_id <= 12
     * </code>
     *
     * @param     mixed $coverPhotoId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AlbumQuery The current query, for fluid interface
     */
    public function filterByCoverPhotoId($coverPhotoId = null, $comparison = null)
    {
        if (is_array($coverPhotoId)) {
            $useMinMax = false;
            if (isset($coverPhotoId['min'])) {
                $this->addUsingAlias(AlbumPeer::COVER_PHOTO_ID, $coverPhotoId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($coverPhotoId['max'])) {
                $this->addUsingAlias(AlbumPeer::COVER_PHOTO_ID, $coverPhotoId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AlbumPeer::COVER_PHOTO_ID, $coverPhotoId, $comparison);
    }

    /**
     * Filter the query on the rank column
     *
     * Example usage:
     * <code>
     * $query->filterByRank(1234); // WHERE rank = 1234
     * $query->filterByRank(array(12, 34)); // WHERE rank IN (12, 34)
     * $query->filterByRank(array('min' => 12)); // WHERE rank >= 12
     * $query->filterByRank(array('max' => 12)); // WHERE rank <= 12
     * </code>
     *
     * @param     mixed $rank The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AlbumQuery The current query, for fluid interface
     */
    public function filterByRank($rank = null, $comparison = null)
    {
        if (is_array($rank)) {
            $useMinMax = false;
            if (isset($rank['min'])) {
                $this->addUsingAlias(AlbumPeer::RANK, $rank['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rank['max'])) {
                $this->addUsingAlias(AlbumPeer::RANK, $rank['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AlbumPeer::RANK, $rank, $comparison);
    }

    /**
     * Filter the query on the is_public column
     *
     * Example usage:
     * <code>
     * $query->filterByIsPublic(1234); // WHERE is_public = 1234
     * $query->filterByIsPublic(array(12, 34)); // WHERE is_public IN (12, 34)
     * $query->filterByIsPublic(array('min' => 12)); // WHERE is_public >= 12
     * $query->filterByIsPublic(array('max' => 12)); // WHERE is_public <= 12
     * </code>
     *
     * @param     mixed $isPublic The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AlbumQuery The current query, for fluid interface
     */
    public function filterByIsPublic($isPublic = null, $comparison = null)
    {
        if (is_array($isPublic)) {
            $useMinMax = false;
            if (isset($isPublic['min'])) {
                $this->addUsingAlias(AlbumPeer::IS_PUBLIC, $isPublic['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($isPublic['max'])) {
                $this->addUsingAlias(AlbumPeer::IS_PUBLIC, $isPublic['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AlbumPeer::IS_PUBLIC, $isPublic, $comparison);
    }

    /**
     * Filter the query on the max_width column
     *
     * Example usage:
     * <code>
     * $query->filterByMaxWidth(1234); // WHERE max_width = 1234
     * $query->filterByMaxWidth(array(12, 34)); // WHERE max_width IN (12, 34)
     * $query->filterByMaxWidth(array('min' => 12)); // WHERE max_width >= 12
     * $query->filterByMaxWidth(array('max' => 12)); // WHERE max_width <= 12
     * </code>
     *
     * @param     mixed $maxWidth The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AlbumQuery The current query, for fluid interface
     */
    public function filterByMaxWidth($maxWidth = null, $comparison = null)
    {
        if (is_array($maxWidth)) {
            $useMinMax = false;
            if (isset($maxWidth['min'])) {
                $this->addUsingAlias(AlbumPeer::MAX_WIDTH, $maxWidth['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($maxWidth['max'])) {
                $this->addUsingAlias(AlbumPeer::MAX_WIDTH, $maxWidth['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AlbumPeer::MAX_WIDTH, $maxWidth, $comparison);
    }

    /**
     * Filter the query on the max_height column
     *
     * Example usage:
     * <code>
     * $query->filterByMaxHeight(1234); // WHERE max_height = 1234
     * $query->filterByMaxHeight(array(12, 34)); // WHERE max_height IN (12, 34)
     * $query->filterByMaxHeight(array('min' => 12)); // WHERE max_height >= 12
     * $query->filterByMaxHeight(array('max' => 12)); // WHERE max_height <= 12
     * </code>
     *
     * @param     mixed $maxHeight The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AlbumQuery The current query, for fluid interface
     */
    public function filterByMaxHeight($maxHeight = null, $comparison = null)
    {
        if (is_array($maxHeight)) {
            $useMinMax = false;
            if (isset($maxHeight['min'])) {
                $this->addUsingAlias(AlbumPeer::MAX_HEIGHT, $maxHeight['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($maxHeight['max'])) {
                $this->addUsingAlias(AlbumPeer::MAX_HEIGHT, $maxHeight['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AlbumPeer::MAX_HEIGHT, $maxHeight, $comparison);
    }

    /**
     * Filter the query by a related Gallery object
     *
     * @param   Gallery|PropelObjectCollection $gallery The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 AlbumQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByGallery($gallery, $comparison = null)
    {
        if ($gallery instanceof Gallery) {
            return $this
                ->addUsingAlias(AlbumPeer::GALLERY_ID, $gallery->getId(), $comparison);
        } elseif ($gallery instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(AlbumPeer::GALLERY_ID, $gallery->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByGallery() only accepts arguments of type Gallery or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Gallery relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return AlbumQuery The current query, for fluid interface
     */
    public function joinGallery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Gallery');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Gallery');
        }

        return $this;
    }

    /**
     * Use the Gallery relation Gallery object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   GalleryQuery A secondary query class using the current class as primary query
     */
    public function useGalleryQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinGallery($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Gallery', 'GalleryQuery');
    }

    /**
     * Filter the query by a related Photo object
     *
     * @param   Photo|PropelObjectCollection $photo  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 AlbumQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPhoto($photo, $comparison = null)
    {
        if ($photo instanceof Photo) {
            return $this
                ->addUsingAlias(AlbumPeer::ID, $photo->getAlbumId(), $comparison);
        } elseif ($photo instanceof PropelObjectCollection) {
            return $this
                ->usePhotoQuery()
                ->filterByPrimaryKeys($photo->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPhoto() only accepts arguments of type Photo or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Photo relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return AlbumQuery The current query, for fluid interface
     */
    public function joinPhoto($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Photo');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Photo');
        }

        return $this;
    }

    /**
     * Use the Photo relation Photo object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   PhotoQuery A secondary query class using the current class as primary query
     */
    public function usePhotoQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPhoto($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Photo', 'PhotoQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Album $album Object to remove from the list of results
     *
     * @return AlbumQuery The current query, for fluid interface
     */
    public function prune($album = null)
    {
        if ($album) {
            $this->addUsingAlias(AlbumPeer::ID, $album->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
