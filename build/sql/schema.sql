
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

-- ---------------------------------------------------------------------
-- acl_permission
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `acl_permission`;

CREATE TABLE `acl_permission`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `module` VARCHAR(20),
    `role_id` INTEGER,
    `resource` VARCHAR(500),
    `actions` TEXT,
    `permission` INTEGER DEFAULT 0,
    PRIMARY KEY (`id`),
    INDEX `FK_acl_permissions` (`role_id`),
    CONSTRAINT `acl_permission_ibfk_1`
        FOREIGN KEY (`role_id`)
        REFERENCES `acl_role` (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- acl_role
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `acl_role`;

CREATE TABLE `acl_role`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(20) NOT NULL,
    `parent_role` INTEGER,
    `is_front_end` TINYINT DEFAULT 0,
    PRIMARY KEY (`id`),
    INDEX `acl_roles_ibfk_1` (`parent_role`),
    CONSTRAINT `acl_role_ibfk_1`
        FOREIGN KEY (`parent_role`)
        REFERENCES `acl_role` (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- admin
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `admin`;

CREATE TABLE `admin`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `first_name` VARCHAR(50) NOT NULL,
    `last_name` VARCHAR(50) NOT NULL,
    `username` VARCHAR(20) NOT NULL,
    `password` VARCHAR(20) NOT NULL,
    `email` VARCHAR(100) NOT NULL,
    `role` INTEGER,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `uniq_admin_email` (`email`),
    INDEX `FK_admins_roles` (`role`),
    CONSTRAINT `admin_ibfk_1`
        FOREIGN KEY (`role`)
        REFERENCES `acl_role` (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- album
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `album`;

CREATE TABLE `album`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `title` VARCHAR(500) NOT NULL,
    `title_slug` VARCHAR(255) NOT NULL,
    `description` VARCHAR(500),
    `gallery_id` INTEGER NOT NULL,
    `cover_photo_id` INTEGER,
    `rank` INTEGER DEFAULT 1,
    `is_public` INTEGER DEFAULT 0,
    `max_width` INTEGER,
    `max_height` INTEGER,
    PRIMARY KEY (`id`),
    INDEX `FK_album` (`gallery_id`),
    CONSTRAINT `album_ibfk_1`
        FOREIGN KEY (`gallery_id`)
        REFERENCES `gallery` (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- file
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `file`;

CREATE TABLE `file`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `filename` VARCHAR(255) NOT NULL,
    `mimetype` VARCHAR(50),
    `size` INTEGER,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- files_tracker
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `files_tracker`;

CREATE TABLE `files_tracker`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `referer_url` VARCHAR(500),
    `ip_addess` VARCHAR(20),
    `fileName` VARCHAR(500),
    `downloaded_time` DATETIME,
    PRIMARY KEY (`id`),
    INDEX `ip_addess` (`ip_addess`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- gallery
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `gallery`;

CREATE TABLE `gallery`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `title` VARCHAR(500) NOT NULL,
    `title_slug` VARCHAR(500) NOT NULL,
    `description` TEXT,
    `created` DATETIME,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- ip_to_country
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `ip_to_country`;

CREATE TABLE `ip_to_country`
(
    `ID` INTEGER NOT NULL,
    `ip_from` VARCHAR(100),
    `ip_to` VARCHAR(100),
    `sigla` VARCHAR(255),
    `code` VARCHAR(255),
    `country` VARCHAR(255),
    PRIMARY KEY (`ID`),
    INDEX `ipranges` (`ip_from`, `ip_to`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- journal
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `journal`;

CREATE TABLE `journal`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `title` VARCHAR(1024) NOT NULL,
    `title_slug` VARCHAR(1024) NOT NULL,
    `text_abstract` TEXT,
    `show_title` TINYINT DEFAULT 1,
    `show_abstract` TINYINT DEFAULT 1,
    `show_fulltext` TINYINT DEFAULT 1,
    `show_file_upload` TINYINT DEFAULT 1,
    `post_order_type` VARCHAR(50) DEFAULT 'rank',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- journal_post
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `journal_post`;

CREATE TABLE `journal_post`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `title` VARCHAR(1024) NOT NULL,
    `title_slug` VARCHAR(1024) NOT NULL,
    `text_abstract` TEXT,
    `content` TEXT,
    `tags` VARCHAR(500),
    `creator_user_id` INTEGER,
    `created` DATETIME,
    `editor_user_id` INTEGER,
    `edited` DATETIME,
    `start_date` DATETIME,
    `end_date` DATETIME,
    `rank` INTEGER,
    `is_public` TINYINT,
    `journal_id` INTEGER NOT NULL,
    `file_id` INTEGER,
    `file_type` VARCHAR(10) NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `journal_id` (`journal_id`),
    INDEX `FK_journal_post` (`file_id`),
    INDEX `FK_journal_post_creator` (`creator_user_id`),
    INDEX `FK_journal_post_editor` (`editor_user_id`),
    CONSTRAINT `journal_post_ibfk_1`
        FOREIGN KEY (`journal_id`)
        REFERENCES `journal` (`id`),
    CONSTRAINT `journal_post_ibfk_2`
        FOREIGN KEY (`file_id`)
        REFERENCES `file` (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- language
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `language`;

CREATE TABLE `language`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `lang` VARCHAR(10),
    `locale` VARCHAR(10),
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- photo
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `photo`;

CREATE TABLE `photo`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `album_id` INTEGER NOT NULL,
    `file_id` INTEGER NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `FK_photos` (`album_id`),
    INDEX `FK_photos_files` (`file_id`),
    CONSTRAINT `photo_ibfk_1`
        FOREIGN KEY (`album_id`)
        REFERENCES `album` (`id`),
    CONSTRAINT `photo_ibfk_2`
        FOREIGN KEY (`file_id`)
        REFERENCES `file` (`id`)
) ENGINE=InnoDB;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
