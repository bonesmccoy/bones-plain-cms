<?php

class Bones_Acl_Acl extends Zend_Acl {

    private static $_instance;
    private $_resources_tree = array();
    private $_cache;
    
    public static function getInstance(){

        if (!is_object(self::$_instance)) {
            self::$_instance = new Bones_Acl_Acl();
        }
        return self::$_instance;
    }
    
    private function __construct() {


        $this->loadRoles();
        $this->loadResources();
        $this->setDefaults();
        $permissions = AclPermissionQuery::create()->find();
        foreach ($permissions as $p) {
            //$p = new AclPermission();
            $actions = is_null($p->getActions()) ? null : unserialize($p->getActions());
            if (!($p->getAclRole() instanceof AclRole))
                continue;
            $roleName = $p->getAclRole()->getName();
            if ($p->getPermission() == 0) {
                $this->deny($roleName, $p->getResource(), $actions);
            } else {
                $this->allow($roleName, $p->getResource(), $actions);
            }
        }
    }

    private function setDefaults() {

        $this->allow("superadmin");
        $this->allow(null, 'admin:admin_login');
        $this->allow(null, 'admin:admin_error');
        $this->allow(null, 'admin:admin_index');

        $this->allow(null, 'default:error');
        $this->allow(null, 'default:index');
    }

    private function loadRoles() {

        $roles = AclRoleQuery::Create()->find();
        foreach ($roles as $role) {
            if ($this->role_exists($role->getName()))
                continue;
            //$role = new AclRole();
            $parent_role = $role->getAclRoleRelatedByParentRole();
            if ($parent_role instanceof AclRole) {
                if (!$this->role_exists($parent_role->getName()))
                    $this->addRole(new Zend_Acl_Role($parent_role->getName()));
                $this->addRole(new Zend_Acl_Role($role->getName()), $parent_role->getName());
            } else {
                $this->addRole(new Zend_Acl_Role($role->getName()));
            }
        }
    }

    private function loadResources() {
        $res = $this->_getResourcesTree();
        foreach ($res as $module => $controllers) {
            foreach ($controllers as $controller => $actions) {
                $this->add(new Zend_Acl_Resource($module . ":" . $controller));
            }
        }
    }

    private function role_exists($role) {
        return in_array($role, $this->getRoles());
    }

    public function isUserAllowed($role = null, $resource = null, $action = null) {

        try {
            $this->get($resource);
            return $this->isAllowed($role, $resource, $action);
        } catch (Exception $e) {
            return false;
        }
    }

    public function getAllowedResources(Bones_Auth_Base $auth, $module) {
        $components = array();
        $permissions = AclPermissionQuery::create()->filterByModule($module)->filterByPermission(1)->filterByRoleId($auth->getRoleId())->find();
        $role = $auth->getRole();
        foreach ($permissions as $p) {
            $actions = unserialize($p->getActions());

            if ($this->isUserAllowed($role, $p->getResource(), 'on_menu')) {
                $res = str_replace("$module:", "", $p->getResource());
                $components[] = $res;
            }

        }
        return $components;
    }
    
    private function _getResourcesTree(){

        if (!empty($this->_resources_tree)) return $this->_resources_tree;
        $cache = Bones_Cache::get("bones");
        $cached_resources_tree = $cache->load("resources_tree");

        if (!empty($cached_resources_tree)) {
            $this->_resources_tree = $cached_resources_tree;
            return $cached_resources_tree;
        }

        $front = Zend_Controller_Front::getInstance();
        $acl = array();
        $module_dir = $front->getModuleDirectory(). "/../";
        foreach (scandir($module_dir) as $module) {
            if (in_array($module, array(".", ".."))) continue;
            $module_controller_path = $front->getModuleDirectory() . "/../$module/controllers/";
            foreach (scandir($module_controller_path) as $file) {
                if (in_array($file, array(".", ".."))) continue;
                if (strstr($file, "Controller.php") !== false) {
                    include_once $module_controller_path .  $file;
                    foreach (get_declared_classes() as $class) {
                        if (is_subclass_of($class, 'Zend_Controller_Action')) {
                            $controller = strtolower(substr($class, 0, strpos($class, "Controller")));
                            $cname = explode("_", $controller);
                            $actions = array();
                            foreach (get_class_methods($class) as $action) {
                                if (strstr($action, "Action") !== false) {
                                    $actions[] = $action;
                                }
                            }
                        }
                    }
                    $acl[$module][$controller] = $actions;
                }
            }
        }
        $this->_resources_tree = $acl;
        $cache->save($acl);
        return $acl;
    }
    
    public function getResourcesTree() {
        return $this->_resources_tree;
    }
    
}
