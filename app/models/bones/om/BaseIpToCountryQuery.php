<?php


/**
 * Base class that represents a query for the 'ip_to_country' table.
 *
 *
 *
 * @method IpToCountryQuery orderById($order = Criteria::ASC) Order by the ID column
 * @method IpToCountryQuery orderByIpFrom($order = Criteria::ASC) Order by the ip_from column
 * @method IpToCountryQuery orderByIpTo($order = Criteria::ASC) Order by the ip_to column
 * @method IpToCountryQuery orderBySigla($order = Criteria::ASC) Order by the sigla column
 * @method IpToCountryQuery orderByCode($order = Criteria::ASC) Order by the code column
 * @method IpToCountryQuery orderByCountry($order = Criteria::ASC) Order by the country column
 *
 * @method IpToCountryQuery groupById() Group by the ID column
 * @method IpToCountryQuery groupByIpFrom() Group by the ip_from column
 * @method IpToCountryQuery groupByIpTo() Group by the ip_to column
 * @method IpToCountryQuery groupBySigla() Group by the sigla column
 * @method IpToCountryQuery groupByCode() Group by the code column
 * @method IpToCountryQuery groupByCountry() Group by the country column
 *
 * @method IpToCountryQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method IpToCountryQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method IpToCountryQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method IpToCountry findOne(PropelPDO $con = null) Return the first IpToCountry matching the query
 * @method IpToCountry findOneOrCreate(PropelPDO $con = null) Return the first IpToCountry matching the query, or a new IpToCountry object populated from the query conditions when no match is found
 *
 * @method IpToCountry findOneByIpFrom(string $ip_from) Return the first IpToCountry filtered by the ip_from column
 * @method IpToCountry findOneByIpTo(string $ip_to) Return the first IpToCountry filtered by the ip_to column
 * @method IpToCountry findOneBySigla(string $sigla) Return the first IpToCountry filtered by the sigla column
 * @method IpToCountry findOneByCode(string $code) Return the first IpToCountry filtered by the code column
 * @method IpToCountry findOneByCountry(string $country) Return the first IpToCountry filtered by the country column
 *
 * @method array findById(int $ID) Return IpToCountry objects filtered by the ID column
 * @method array findByIpFrom(string $ip_from) Return IpToCountry objects filtered by the ip_from column
 * @method array findByIpTo(string $ip_to) Return IpToCountry objects filtered by the ip_to column
 * @method array findBySigla(string $sigla) Return IpToCountry objects filtered by the sigla column
 * @method array findByCode(string $code) Return IpToCountry objects filtered by the code column
 * @method array findByCountry(string $country) Return IpToCountry objects filtered by the country column
 *
 * @package    propel.generator.ORM.om
 */
abstract class BaseIpToCountryQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseIpToCountryQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'ORM';
        }
        if (null === $modelName) {
            $modelName = 'IpToCountry';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new IpToCountryQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   IpToCountryQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return IpToCountryQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof IpToCountryQuery) {
            return $criteria;
        }
        $query = new IpToCountryQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   IpToCountry|IpToCountry[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = IpToCountryPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(IpToCountryPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 IpToCountry A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 IpToCountry A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `ID`, `ip_from`, `ip_to`, `sigla`, `code`, `country` FROM `ip_to_country` WHERE `ID` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new IpToCountry();
            $obj->hydrate($row);
            IpToCountryPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return IpToCountry|IpToCountry[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|IpToCountry[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return IpToCountryQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(IpToCountryPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return IpToCountryQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(IpToCountryPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the ID column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE ID = 1234
     * $query->filterById(array(12, 34)); // WHERE ID IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE ID >= 12
     * $query->filterById(array('max' => 12)); // WHERE ID <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return IpToCountryQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(IpToCountryPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(IpToCountryPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(IpToCountryPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the ip_from column
     *
     * Example usage:
     * <code>
     * $query->filterByIpFrom('fooValue');   // WHERE ip_from = 'fooValue'
     * $query->filterByIpFrom('%fooValue%'); // WHERE ip_from LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ipFrom The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return IpToCountryQuery The current query, for fluid interface
     */
    public function filterByIpFrom($ipFrom = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ipFrom)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $ipFrom)) {
                $ipFrom = str_replace('*', '%', $ipFrom);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(IpToCountryPeer::IP_FROM, $ipFrom, $comparison);
    }

    /**
     * Filter the query on the ip_to column
     *
     * Example usage:
     * <code>
     * $query->filterByIpTo('fooValue');   // WHERE ip_to = 'fooValue'
     * $query->filterByIpTo('%fooValue%'); // WHERE ip_to LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ipTo The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return IpToCountryQuery The current query, for fluid interface
     */
    public function filterByIpTo($ipTo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ipTo)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $ipTo)) {
                $ipTo = str_replace('*', '%', $ipTo);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(IpToCountryPeer::IP_TO, $ipTo, $comparison);
    }

    /**
     * Filter the query on the sigla column
     *
     * Example usage:
     * <code>
     * $query->filterBySigla('fooValue');   // WHERE sigla = 'fooValue'
     * $query->filterBySigla('%fooValue%'); // WHERE sigla LIKE '%fooValue%'
     * </code>
     *
     * @param     string $sigla The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return IpToCountryQuery The current query, for fluid interface
     */
    public function filterBySigla($sigla = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($sigla)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $sigla)) {
                $sigla = str_replace('*', '%', $sigla);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(IpToCountryPeer::SIGLA, $sigla, $comparison);
    }

    /**
     * Filter the query on the code column
     *
     * Example usage:
     * <code>
     * $query->filterByCode('fooValue');   // WHERE code = 'fooValue'
     * $query->filterByCode('%fooValue%'); // WHERE code LIKE '%fooValue%'
     * </code>
     *
     * @param     string $code The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return IpToCountryQuery The current query, for fluid interface
     */
    public function filterByCode($code = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($code)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $code)) {
                $code = str_replace('*', '%', $code);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(IpToCountryPeer::CODE, $code, $comparison);
    }

    /**
     * Filter the query on the country column
     *
     * Example usage:
     * <code>
     * $query->filterByCountry('fooValue');   // WHERE country = 'fooValue'
     * $query->filterByCountry('%fooValue%'); // WHERE country LIKE '%fooValue%'
     * </code>
     *
     * @param     string $country The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return IpToCountryQuery The current query, for fluid interface
     */
    public function filterByCountry($country = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($country)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $country)) {
                $country = str_replace('*', '%', $country);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(IpToCountryPeer::COUNTRY, $country, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   IpToCountry $ipToCountry Object to remove from the list of results
     *
     * @return IpToCountryQuery The current query, for fluid interface
     */
    public function prune($ipToCountry = null)
    {
        if ($ipToCountry) {
            $this->addUsingAlias(IpToCountryPeer::ID, $ipToCountry->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
