<?php


/**
 * Base class that represents a query for the 'acl_role' table.
 *
 *
 *
 * @method AclRoleQuery orderById($order = Criteria::ASC) Order by the id column
 * @method AclRoleQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method AclRoleQuery orderByParentRole($order = Criteria::ASC) Order by the parent_role column
 * @method AclRoleQuery orderByIsFrontEnd($order = Criteria::ASC) Order by the is_front_end column
 *
 * @method AclRoleQuery groupById() Group by the id column
 * @method AclRoleQuery groupByName() Group by the name column
 * @method AclRoleQuery groupByParentRole() Group by the parent_role column
 * @method AclRoleQuery groupByIsFrontEnd() Group by the is_front_end column
 *
 * @method AclRoleQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method AclRoleQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method AclRoleQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method AclRoleQuery leftJoinAclRoleRelatedByParentRole($relationAlias = null) Adds a LEFT JOIN clause to the query using the AclRoleRelatedByParentRole relation
 * @method AclRoleQuery rightJoinAclRoleRelatedByParentRole($relationAlias = null) Adds a RIGHT JOIN clause to the query using the AclRoleRelatedByParentRole relation
 * @method AclRoleQuery innerJoinAclRoleRelatedByParentRole($relationAlias = null) Adds a INNER JOIN clause to the query using the AclRoleRelatedByParentRole relation
 *
 * @method AclRoleQuery leftJoinAclPermission($relationAlias = null) Adds a LEFT JOIN clause to the query using the AclPermission relation
 * @method AclRoleQuery rightJoinAclPermission($relationAlias = null) Adds a RIGHT JOIN clause to the query using the AclPermission relation
 * @method AclRoleQuery innerJoinAclPermission($relationAlias = null) Adds a INNER JOIN clause to the query using the AclPermission relation
 *
 * @method AclRoleQuery leftJoinAclRoleRelatedById($relationAlias = null) Adds a LEFT JOIN clause to the query using the AclRoleRelatedById relation
 * @method AclRoleQuery rightJoinAclRoleRelatedById($relationAlias = null) Adds a RIGHT JOIN clause to the query using the AclRoleRelatedById relation
 * @method AclRoleQuery innerJoinAclRoleRelatedById($relationAlias = null) Adds a INNER JOIN clause to the query using the AclRoleRelatedById relation
 *
 * @method AclRoleQuery leftJoinAdmin($relationAlias = null) Adds a LEFT JOIN clause to the query using the Admin relation
 * @method AclRoleQuery rightJoinAdmin($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Admin relation
 * @method AclRoleQuery innerJoinAdmin($relationAlias = null) Adds a INNER JOIN clause to the query using the Admin relation
 *
 * @method AclRole findOne(PropelPDO $con = null) Return the first AclRole matching the query
 * @method AclRole findOneOrCreate(PropelPDO $con = null) Return the first AclRole matching the query, or a new AclRole object populated from the query conditions when no match is found
 *
 * @method AclRole findOneByName(string $name) Return the first AclRole filtered by the name column
 * @method AclRole findOneByParentRole(int $parent_role) Return the first AclRole filtered by the parent_role column
 * @method AclRole findOneByIsFrontEnd(int $is_front_end) Return the first AclRole filtered by the is_front_end column
 *
 * @method array findById(int $id) Return AclRole objects filtered by the id column
 * @method array findByName(string $name) Return AclRole objects filtered by the name column
 * @method array findByParentRole(int $parent_role) Return AclRole objects filtered by the parent_role column
 * @method array findByIsFrontEnd(int $is_front_end) Return AclRole objects filtered by the is_front_end column
 *
 * @package    propel.generator.ORM.om
 */
abstract class BaseAclRoleQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseAclRoleQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'ORM';
        }
        if (null === $modelName) {
            $modelName = 'AclRole';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new AclRoleQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   AclRoleQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return AclRoleQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof AclRoleQuery) {
            return $criteria;
        }
        $query = new AclRoleQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   AclRole|AclRole[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = AclRolePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(AclRolePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 AclRole A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 AclRole A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `name`, `parent_role`, `is_front_end` FROM `acl_role` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new AclRole();
            $obj->hydrate($row);
            AclRolePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return AclRole|AclRole[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|AclRole[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return AclRoleQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(AclRolePeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return AclRoleQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(AclRolePeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AclRoleQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(AclRolePeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(AclRolePeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AclRolePeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AclRoleQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $name)) {
                $name = str_replace('*', '%', $name);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AclRolePeer::NAME, $name, $comparison);
    }

    /**
     * Filter the query on the parent_role column
     *
     * Example usage:
     * <code>
     * $query->filterByParentRole(1234); // WHERE parent_role = 1234
     * $query->filterByParentRole(array(12, 34)); // WHERE parent_role IN (12, 34)
     * $query->filterByParentRole(array('min' => 12)); // WHERE parent_role >= 12
     * $query->filterByParentRole(array('max' => 12)); // WHERE parent_role <= 12
     * </code>
     *
     * @see       filterByAclRoleRelatedByParentRole()
     *
     * @param     mixed $parentRole The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AclRoleQuery The current query, for fluid interface
     */
    public function filterByParentRole($parentRole = null, $comparison = null)
    {
        if (is_array($parentRole)) {
            $useMinMax = false;
            if (isset($parentRole['min'])) {
                $this->addUsingAlias(AclRolePeer::PARENT_ROLE, $parentRole['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($parentRole['max'])) {
                $this->addUsingAlias(AclRolePeer::PARENT_ROLE, $parentRole['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AclRolePeer::PARENT_ROLE, $parentRole, $comparison);
    }

    /**
     * Filter the query on the is_front_end column
     *
     * Example usage:
     * <code>
     * $query->filterByIsFrontEnd(1234); // WHERE is_front_end = 1234
     * $query->filterByIsFrontEnd(array(12, 34)); // WHERE is_front_end IN (12, 34)
     * $query->filterByIsFrontEnd(array('min' => 12)); // WHERE is_front_end >= 12
     * $query->filterByIsFrontEnd(array('max' => 12)); // WHERE is_front_end <= 12
     * </code>
     *
     * @param     mixed $isFrontEnd The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AclRoleQuery The current query, for fluid interface
     */
    public function filterByIsFrontEnd($isFrontEnd = null, $comparison = null)
    {
        if (is_array($isFrontEnd)) {
            $useMinMax = false;
            if (isset($isFrontEnd['min'])) {
                $this->addUsingAlias(AclRolePeer::IS_FRONT_END, $isFrontEnd['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($isFrontEnd['max'])) {
                $this->addUsingAlias(AclRolePeer::IS_FRONT_END, $isFrontEnd['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AclRolePeer::IS_FRONT_END, $isFrontEnd, $comparison);
    }

    /**
     * Filter the query by a related AclRole object
     *
     * @param   AclRole|PropelObjectCollection $aclRole The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 AclRoleQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByAclRoleRelatedByParentRole($aclRole, $comparison = null)
    {
        if ($aclRole instanceof AclRole) {
            return $this
                ->addUsingAlias(AclRolePeer::PARENT_ROLE, $aclRole->getId(), $comparison);
        } elseif ($aclRole instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(AclRolePeer::PARENT_ROLE, $aclRole->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByAclRoleRelatedByParentRole() only accepts arguments of type AclRole or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the AclRoleRelatedByParentRole relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return AclRoleQuery The current query, for fluid interface
     */
    public function joinAclRoleRelatedByParentRole($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('AclRoleRelatedByParentRole');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'AclRoleRelatedByParentRole');
        }

        return $this;
    }

    /**
     * Use the AclRoleRelatedByParentRole relation AclRole object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   AclRoleQuery A secondary query class using the current class as primary query
     */
    public function useAclRoleRelatedByParentRoleQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinAclRoleRelatedByParentRole($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'AclRoleRelatedByParentRole', 'AclRoleQuery');
    }

    /**
     * Filter the query by a related AclPermission object
     *
     * @param   AclPermission|PropelObjectCollection $aclPermission  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 AclRoleQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByAclPermission($aclPermission, $comparison = null)
    {
        if ($aclPermission instanceof AclPermission) {
            return $this
                ->addUsingAlias(AclRolePeer::ID, $aclPermission->getRoleId(), $comparison);
        } elseif ($aclPermission instanceof PropelObjectCollection) {
            return $this
                ->useAclPermissionQuery()
                ->filterByPrimaryKeys($aclPermission->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByAclPermission() only accepts arguments of type AclPermission or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the AclPermission relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return AclRoleQuery The current query, for fluid interface
     */
    public function joinAclPermission($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('AclPermission');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'AclPermission');
        }

        return $this;
    }

    /**
     * Use the AclPermission relation AclPermission object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   AclPermissionQuery A secondary query class using the current class as primary query
     */
    public function useAclPermissionQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinAclPermission($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'AclPermission', 'AclPermissionQuery');
    }

    /**
     * Filter the query by a related AclRole object
     *
     * @param   AclRole|PropelObjectCollection $aclRole  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 AclRoleQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByAclRoleRelatedById($aclRole, $comparison = null)
    {
        if ($aclRole instanceof AclRole) {
            return $this
                ->addUsingAlias(AclRolePeer::ID, $aclRole->getParentRole(), $comparison);
        } elseif ($aclRole instanceof PropelObjectCollection) {
            return $this
                ->useAclRoleRelatedByIdQuery()
                ->filterByPrimaryKeys($aclRole->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByAclRoleRelatedById() only accepts arguments of type AclRole or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the AclRoleRelatedById relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return AclRoleQuery The current query, for fluid interface
     */
    public function joinAclRoleRelatedById($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('AclRoleRelatedById');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'AclRoleRelatedById');
        }

        return $this;
    }

    /**
     * Use the AclRoleRelatedById relation AclRole object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   AclRoleQuery A secondary query class using the current class as primary query
     */
    public function useAclRoleRelatedByIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinAclRoleRelatedById($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'AclRoleRelatedById', 'AclRoleQuery');
    }

    /**
     * Filter the query by a related Admin object
     *
     * @param   Admin|PropelObjectCollection $admin  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 AclRoleQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByAdmin($admin, $comparison = null)
    {
        if ($admin instanceof Admin) {
            return $this
                ->addUsingAlias(AclRolePeer::ID, $admin->getRole(), $comparison);
        } elseif ($admin instanceof PropelObjectCollection) {
            return $this
                ->useAdminQuery()
                ->filterByPrimaryKeys($admin->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByAdmin() only accepts arguments of type Admin or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Admin relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return AclRoleQuery The current query, for fluid interface
     */
    public function joinAdmin($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Admin');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Admin');
        }

        return $this;
    }

    /**
     * Use the Admin relation Admin object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   AdminQuery A secondary query class using the current class as primary query
     */
    public function useAdminQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinAdmin($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Admin', 'AdminQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   AclRole $aclRole Object to remove from the list of results
     *
     * @return AclRoleQuery The current query, for fluid interface
     */
    public function prune($aclRole = null)
    {
        if ($aclRole) {
            $this->addUsingAlias(AclRolePeer::ID, $aclRole->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
