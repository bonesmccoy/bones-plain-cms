<?php



/**
 * This class defines the structure of the 'album' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.ORM.map
 */
class AlbumTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'ORM.map.AlbumTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('album');
        $this->setPhpName('Album');
        $this->setClassname('Album');
        $this->setPackage('ORM');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('title', 'Title', 'VARCHAR', true, 500, null);
        $this->addColumn('title_slug', 'TitleSlug', 'VARCHAR', true, 255, null);
        $this->addColumn('description', 'Description', 'VARCHAR', false, 500, null);
        $this->addForeignKey('gallery_id', 'GalleryId', 'INTEGER', 'gallery', 'id', true, null, null);
        $this->addColumn('cover_photo_id', 'CoverPhotoId', 'INTEGER', false, null, null);
        $this->addColumn('rank', 'Rank', 'INTEGER', false, null, 1);
        $this->addColumn('is_public', 'IsPublic', 'INTEGER', false, null, 0);
        $this->addColumn('max_width', 'MaxWidth', 'INTEGER', false, null, null);
        $this->addColumn('max_height', 'MaxHeight', 'INTEGER', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Gallery', 'Gallery', RelationMap::MANY_TO_ONE, array('gallery_id' => 'id', ), null, null);
        $this->addRelation('Photo', 'Photo', RelationMap::ONE_TO_MANY, array('id' => 'album_id', ), null, null, 'Photos');
    } // buildRelations()

} // AlbumTableMap
