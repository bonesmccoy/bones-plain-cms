<?php



/**
 * This class defines the structure of the 'acl_permission' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.ORM.map
 */
class AclPermissionTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'ORM.map.AclPermissionTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('acl_permission');
        $this->setPhpName('AclPermission');
        $this->setClassname('AclPermission');
        $this->setPackage('ORM');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('module', 'Module', 'VARCHAR', false, 20, null);
        $this->addForeignKey('role_id', 'RoleId', 'INTEGER', 'acl_role', 'id', false, null, null);
        $this->addColumn('resource', 'Resource', 'VARCHAR', false, 500, null);
        $this->addColumn('actions', 'Actions', 'LONGVARCHAR', false, null, null);
        $this->addColumn('permission', 'Permission', 'INTEGER', false, null, 0);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('AclRole', 'AclRole', RelationMap::MANY_TO_ONE, array('role_id' => 'id', ), null, null);
    } // buildRelations()

} // AclPermissionTableMap
