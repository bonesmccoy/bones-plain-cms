<?php




class IndexController extends Bones_Controller_Cli
{


	/**
	 *	Just run
	 *  php cli.php
	 */
	public function indexAction ()
	{
        echo <<<USAGE
Usage:
    php cli.php <controller_name> <action_name> <key=value>

Examples:
    php cli.php index info` executes the info action of the index controller.
    php cli.php index info KEY=VALUE` adds the parameter to the request.
    php cli.php index info KEY` adds the boolean parameter equals to TRUE to the request.

USAGE;

	}


	/**
	 * php cli.php info
	 */
	public function infoAction ()
	{
		echo <<<info
Usage:
	php cli.php index info
		This information.


info;

	}


	public function errorAction ()
	{
		throw new Exception ("Some error.");
	}
}
