<?php
require_once dirname(__FILE__) . "/../app/configs/init.php";

$config = Bones_Config::load(Bones_Config::WEB);
$application = new Zend_Application(
    null,
    $config
);
Propel::init(APPLICATION_PATH . "/configs/bones-conf.php");
$application->bootstrap()->run();